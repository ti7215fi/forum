# Forum

This application was developed within the module "Web Engineering 2" in the winter semester 2017/2018 by Stefanie Schwarz and Tim Fischer.   
The goal was to develope a forum with threads and posts. There were clearly definied requirements.

## Try it out!

This app is running on http://dbforum.gear.host/   
**Hint:** Sometimes the application is really slow or the service is not available (it's only free hosted - sorry for that).

Logins:

- admin (administrator)
- max (default user)
- susi(administrator)
- iwan (default user)

The password is always **test123**.

## Development environment
- Microsoft Visual Studio 2017 Enterprise
- Microsoft SQL Server Management Studio 17
- Microsoft SQL Server 2017 Express
- Visual Studio Code

## Tech Stack
- Angular 5+
- Bootstrap 3
- ASP.NET MVC
- Entity Framework

## Prerequisite
- npm package manager: https://www.npmjs.com/get-npm
- Angular CLI `npm install -g @angular/cli`

## Install

### Client

Run the following commands inside the Web/ngroot folder.  

```
// install npm packages
npm install

// build angular modules
ng build
```

### Server

If you use Visual Studio too then you have to install the following NuGet packages:

- Microsoft.Typescript.Compiler
- Microsoft.Typescript.MSBuild

1. Install all other NuGet packages defined as dependencies (auto install when you use Visual Studio).
2. Create the database "WE2_Forum"
2. Run the sql script "Forum_Container.edmx.sql" (inside Definitions) to create all tables to db.
3. Run the sql script "init.sql" (inside Data) to init the db with some test data.
4. Create and open an connection to your sql server inside visual studio.
5. Change the "connectionString" inside the config files (Definitions/App.config, line 14 and Web/Web.config, line 76) starting at "data source". (How to find your connection string https://www.c-sharpcorner.com/uploadfile/suthish_nair/how-to-generate-or-find-connection-string-from-visual-studio/)
6. Run your project.

## Troubleshooting

- Sometimes there are problems to run the project with Chrome (auto run). In that case try to use another browser like Firefox or start Chrome manually.
- The default web server for local Microsoft sql servers are IIS. If you run a local sql server, make sure IIS is running on your system.

## Development
Run follow command after changes in angular files:
```
ng build
```