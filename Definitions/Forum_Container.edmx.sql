
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/26/2018 10:53:33
-- Generated from EDMX file: F:\Studium\Master\MA2\Web Engineering 2\MessageBoard\WE2Project\Definitions\Forum_Container.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [WE2_Forum];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ThreadPost]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Posts] DROP CONSTRAINT [FK_ThreadPost];
GO
IF OBJECT_ID(N'[dbo].[FK_ForumThread]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Threads] DROP CONSTRAINT [FK_ForumThread];
GO
IF OBJECT_ID(N'[dbo].[FK_UserForum]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Forums] DROP CONSTRAINT [FK_UserForum];
GO
IF OBJECT_ID(N'[dbo].[FK_UserThread]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Threads] DROP CONSTRAINT [FK_UserThread];
GO
IF OBJECT_ID(N'[dbo].[FK_UserPost]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Posts] DROP CONSTRAINT [FK_UserPost];
GO
IF OBJECT_ID(N'[dbo].[FK_UserRole_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UserRole_Role]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserRole] DROP CONSTRAINT [FK_UserRole_Role];
GO
IF OBJECT_ID(N'[dbo].[FK_MainForumSubForum]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Forums] DROP CONSTRAINT [FK_MainForumSubForum];
GO
IF OBJECT_ID(N'[dbo].[FK_ReadForumRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ForumConfigs] DROP CONSTRAINT [FK_ReadForumRole];
GO
IF OBJECT_ID(N'[dbo].[FK_CreateEditThreadsRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ForumConfigs] DROP CONSTRAINT [FK_CreateEditThreadsRole];
GO
IF OBJECT_ID(N'[dbo].[FK_CreateEditPostsRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ForumConfigs] DROP CONSTRAINT [FK_CreateEditPostsRole];
GO
IF OBJECT_ID(N'[dbo].[FK_ForumForumConfig]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ForumConfigs] DROP CONSTRAINT [FK_ForumForumConfig];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Forums]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Forums];
GO
IF OBJECT_ID(N'[dbo].[Threads]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Threads];
GO
IF OBJECT_ID(N'[dbo].[Posts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Posts];
GO
IF OBJECT_ID(N'[dbo].[ForumConfigs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ForumConfigs];
GO
IF OBJECT_ID(N'[dbo].[UserRole]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserRole];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Forums'
CREATE TABLE [dbo].[Forums] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Type] nvarchar(max)  NOT NULL,
    [Position] int  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [CreatedBy_Id] int  NOT NULL,
    [MainForum_Id] int  NULL
);
GO

-- Creating table 'Threads'
CREATE TABLE [dbo].[Threads] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [DescriptionPlain] nvarchar(max)  NOT NULL,
    [DescriptionFormatted] nvarchar(max)  NOT NULL,
    [Unlocked] bit  NOT NULL,
    [Status] nvarchar(max)  NULL,
    [CreatedAt] datetime  NOT NULL,
    [DeletedAt] datetime  NULL,
    [ArchivedAt] datetime  NULL,
    [Forum_Id] int  NOT NULL,
    [CreatedBy_Id] int  NOT NULL
);
GO

-- Creating table 'Posts'
CREATE TABLE [dbo].[Posts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DescriptionFormatted] nvarchar(max)  NOT NULL,
    [DescriptionPlain] nvarchar(max)  NOT NULL,
    [CreatedAt] datetime  NOT NULL,
    [Thread_Id] int  NOT NULL,
    [CreatedBy_Id] int  NOT NULL
);
GO

-- Creating table 'ForumConfigs'
CREATE TABLE [dbo].[ForumConfigs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FeatureThreadStatus] bit  NOT NULL,
    [RoleCanRead_Id] int  NOT NULL,
    [RoleCanCreateEditThread_Id] int  NOT NULL,
    [RoleCanCreateEditPost_Id] int  NOT NULL,
    [Forum_Id] int  NOT NULL
);
GO

-- Creating table 'UserRole'
CREATE TABLE [dbo].[UserRole] (
    [Users_Id] int  NOT NULL,
    [Roles_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Forums'
ALTER TABLE [dbo].[Forums]
ADD CONSTRAINT [PK_Forums]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Threads'
ALTER TABLE [dbo].[Threads]
ADD CONSTRAINT [PK_Threads]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Posts'
ALTER TABLE [dbo].[Posts]
ADD CONSTRAINT [PK_Posts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ForumConfigs'
ALTER TABLE [dbo].[ForumConfigs]
ADD CONSTRAINT [PK_ForumConfigs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Users_Id], [Roles_Id] in table 'UserRole'
ALTER TABLE [dbo].[UserRole]
ADD CONSTRAINT [PK_UserRole]
    PRIMARY KEY CLUSTERED ([Users_Id], [Roles_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Thread_Id] in table 'Posts'
ALTER TABLE [dbo].[Posts]
ADD CONSTRAINT [FK_ThreadPost]
    FOREIGN KEY ([Thread_Id])
    REFERENCES [dbo].[Threads]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ThreadPost'
CREATE INDEX [IX_FK_ThreadPost]
ON [dbo].[Posts]
    ([Thread_Id]);
GO

-- Creating foreign key on [Forum_Id] in table 'Threads'
ALTER TABLE [dbo].[Threads]
ADD CONSTRAINT [FK_ForumThread]
    FOREIGN KEY ([Forum_Id])
    REFERENCES [dbo].[Forums]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ForumThread'
CREATE INDEX [IX_FK_ForumThread]
ON [dbo].[Threads]
    ([Forum_Id]);
GO

-- Creating foreign key on [CreatedBy_Id] in table 'Forums'
ALTER TABLE [dbo].[Forums]
ADD CONSTRAINT [FK_UserForum]
    FOREIGN KEY ([CreatedBy_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserForum'
CREATE INDEX [IX_FK_UserForum]
ON [dbo].[Forums]
    ([CreatedBy_Id]);
GO

-- Creating foreign key on [CreatedBy_Id] in table 'Threads'
ALTER TABLE [dbo].[Threads]
ADD CONSTRAINT [FK_UserThread]
    FOREIGN KEY ([CreatedBy_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserThread'
CREATE INDEX [IX_FK_UserThread]
ON [dbo].[Threads]
    ([CreatedBy_Id]);
GO

-- Creating foreign key on [CreatedBy_Id] in table 'Posts'
ALTER TABLE [dbo].[Posts]
ADD CONSTRAINT [FK_UserPost]
    FOREIGN KEY ([CreatedBy_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserPost'
CREATE INDEX [IX_FK_UserPost]
ON [dbo].[Posts]
    ([CreatedBy_Id]);
GO

-- Creating foreign key on [Users_Id] in table 'UserRole'
ALTER TABLE [dbo].[UserRole]
ADD CONSTRAINT [FK_UserRole_User]
    FOREIGN KEY ([Users_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Roles_Id] in table 'UserRole'
ALTER TABLE [dbo].[UserRole]
ADD CONSTRAINT [FK_UserRole_Role]
    FOREIGN KEY ([Roles_Id])
    REFERENCES [dbo].[Roles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserRole_Role'
CREATE INDEX [IX_FK_UserRole_Role]
ON [dbo].[UserRole]
    ([Roles_Id]);
GO

-- Creating foreign key on [MainForum_Id] in table 'Forums'
ALTER TABLE [dbo].[Forums]
ADD CONSTRAINT [FK_MainForumSubForum]
    FOREIGN KEY ([MainForum_Id])
    REFERENCES [dbo].[Forums]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MainForumSubForum'
CREATE INDEX [IX_FK_MainForumSubForum]
ON [dbo].[Forums]
    ([MainForum_Id]);
GO

-- Creating foreign key on [RoleCanRead_Id] in table 'ForumConfigs'
ALTER TABLE [dbo].[ForumConfigs]
ADD CONSTRAINT [FK_ReadForumRole]
    FOREIGN KEY ([RoleCanRead_Id])
    REFERENCES [dbo].[Roles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReadForumRole'
CREATE INDEX [IX_FK_ReadForumRole]
ON [dbo].[ForumConfigs]
    ([RoleCanRead_Id]);
GO

-- Creating foreign key on [RoleCanCreateEditThread_Id] in table 'ForumConfigs'
ALTER TABLE [dbo].[ForumConfigs]
ADD CONSTRAINT [FK_CreateEditThreadsRole]
    FOREIGN KEY ([RoleCanCreateEditThread_Id])
    REFERENCES [dbo].[Roles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CreateEditThreadsRole'
CREATE INDEX [IX_FK_CreateEditThreadsRole]
ON [dbo].[ForumConfigs]
    ([RoleCanCreateEditThread_Id]);
GO

-- Creating foreign key on [RoleCanCreateEditPost_Id] in table 'ForumConfigs'
ALTER TABLE [dbo].[ForumConfigs]
ADD CONSTRAINT [FK_CreateEditPostsRole]
    FOREIGN KEY ([RoleCanCreateEditPost_Id])
    REFERENCES [dbo].[Roles]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CreateEditPostsRole'
CREATE INDEX [IX_FK_CreateEditPostsRole]
ON [dbo].[ForumConfigs]
    ([RoleCanCreateEditPost_Id]);
GO

-- Creating foreign key on [Forum_Id] in table 'ForumConfigs'
ALTER TABLE [dbo].[ForumConfigs]
ADD CONSTRAINT [FK_ForumForumConfig]
    FOREIGN KEY ([Forum_Id])
    REFERENCES [dbo].[Forums]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ForumForumConfig'
CREATE INDEX [IX_FK_ForumForumConfig]
ON [dbo].[ForumConfigs]
    ([Forum_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------