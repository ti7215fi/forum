﻿namespace Definitions
{
    public static class UserRole
    {
        public static string ADMIN { get { return "ADMIN"; } }
        public static string DEFAULT_USER { get { return "DEFAULT_USER"; } }
    }
}
