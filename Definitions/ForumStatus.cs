﻿namespace Definitions
{
    public static class ThreadStatus
    {
        public static string REJECTED { get { return "REJECTED"; } }
        public static string REVIEW { get { return "REVIEW"; } }
        public static string ACCEPTED { get { return "ACCEPTED"; } }
    }
}
