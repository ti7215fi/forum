﻿namespace Definitions
{
    public static class ForumType
    {
        public static string ANNOUNCEMENT { get { return "ANNOUNCEMENT"; } }
        public static string PROPOSAL { get { return "PROPOSAL"; } }
        public static string DISCUSSION { get { return "DISCUSSION"; } }
    }
}
