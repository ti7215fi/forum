USE [WE2_Forum]
GO

INSERT INTO [dbo].[Users]
           ([FirstName]
           ,[LastName]
		   ,[UserName]
           ,[Password])
     VALUES
           ('Adam',
           'Administrator',
		   'admin',
		   'test123'),
		    ('Max',
           'Mustermann',
		   'max',
		   'test123'),
		   ('Susi',
           'Sorglos',
		   'susi',
		   'test123'),
		    ('Iwan Iwanowitsch',
           'Iwanov',
		   'iwan',
		   'test123')
GO

INSERT INTO [dbo].[Roles]
           ([Name]
           ,[Description])
     VALUES
           ('ADMIN',
           'Administator'),
           ('DEFAULT_USER',
           'Standard-Benutzer')
GO

INSERT INTO [dbo].[UserRole]
           ([Users_Id]
           ,[Roles_Id])
     VALUES
           (1,
           1),
		   (1,
		   2),
		  (2,
           2),
		  (3,
           1),
		   (3,
		   2),
		  (4,
           2)
GO

INSERT INTO [dbo].[Forums]
           ([Name]
           ,[Description]
           ,[Type]
           ,[Position]
           ,[CreatedAt]
           ,[CreatedBy_Id]
           ,[MainForum_Id])
     VALUES
           ('TestForum'
           ,'Ein Forum zum Test'
           ,'ANNOUNCEMENT'
           ,10
           ,'06-01-2017'
           ,1
           ,null),
		   ('Cooles Forum'
           ,'Ein Forum zum abk�hlen'
           ,'ANNOUNCEMENT'
           ,10
           ,'06-01-2017'
           ,1
           ,null)
GO

INSERT INTO [dbo].[ForumConfigs]
           ([FeatureThreadStatus]
           ,[RoleCanRead_Id]
           ,[RoleCanCreateEditThread_Id]
           ,[RoleCanCreateEditPost_Id]
           ,[Forum_Id])
     VALUES
           (1,
           2,
           2,
           2,
           1),
		  (0,
           2,
           1,
           1,
           2)
GO

INSERT INTO [dbo].[Threads]
           ([Title]
           ,[DescriptionPlain]
		   ,[DescriptionFormatted]
           ,[CreatedAt]
		   ,[Unlocked]
           ,[Forum_Id]
           ,[CreatedBy_Id])
     VALUES
           ('Abstimmung Logo'
           ,'Wir m�ssen noch �ber unser neues Logo diskutieren.'
		   ,'Wir m�ssen noch �ber unser neues <strong>Logo</strong> diskutieren.'
           ,'06-01-2017'
		   ,1
           ,2
           ,1),
		   ('Auswertung der Weihnachtsfeier'
           ,'In diesem Forum soll es um unsere Weihnachstfeier gehen.'
		   ,'In diesem Forum soll es um unsere <strong>Weihnachstfeier</strong> gehen.'
           ,'06-01-2017'
		   ,0
           ,2
           ,2)
GO

INSERT INTO [dbo].[Posts]
           ([DescriptionFormatted]
		   ,[DescriptionPlain]
           ,[CreatedAt]
           ,[Thread_Id]
           ,[CreatedBy_Id])
     VALUES
           ('Ich denke es sollte <strong style="color: green">gr�n</strong> werden.'
		   ,'Ich denke es sollte gr�n werden.'
           ,'06-01-2017'
           ,1
           ,2),
		   ('Naja, gr�n passt nicht so ganz zu unserem <strong style="color: red">roten</strong> T-Shirt :('
		   ,'Naja, gr�n passt nicht so ganz zu unserem roten T-Shirt :('
           ,'06-01-2017'
           ,1
           ,2)
GO
