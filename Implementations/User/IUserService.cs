﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public interface IUserService
    {
        bool IsAdmin(string username);
    }
}
