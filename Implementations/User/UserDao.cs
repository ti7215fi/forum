﻿using Definitions;
using System.Linq;

namespace Implementations
{
    public class UserDao : BaseDao
    {
        private static UserDao _Instance;
        private UserDao() { }

        public static UserDao Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new UserDao();
                }
                return _Instance;
            }
        }

        public User GetUserByUserName(ForumContainer context, string username)
        {
            return context.Users.Where(u => u.UserName == username).FirstOrDefault();
        }

        // ToDo: move this to a user service
        public bool HasRole(ForumContainer context, string username, string role)
        {
            var user = GetUserByUserName(context, username);
            if (user != null)
            {
                return user.Roles.Where(r => r.Name.Equals(role)).FirstOrDefault() != null;
            }
            return false;
        }
    }
}
