﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class UserService : IUserService
    {
        public bool IsAdmin(string username)
        {
            return UserRoleService.Instance.IsUserInRole(username, UserRole.ADMIN);
        }
    }
}
