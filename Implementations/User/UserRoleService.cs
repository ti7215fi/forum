﻿using Definitions;
using System;
using System.Linq;

namespace Implementations
{
    public class UserRoleService
    {
        private static UserRoleService _Instance;
        private UserRoleService() { }

        public static UserRoleService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new UserRoleService();
                }
                return _Instance;
            }
        }

        public readonly static string[] UserRoles = {
            UserRole.ADMIN,
            UserRole.DEFAULT_USER
        };

        public static bool IsValid(string userRole)
        {
            return Array.IndexOf(UserRoles, userRole) > -1;
        }

        public string[] GetRolesForUser(string username)
        {
            using(ForumContainer db = new ForumContainer())
            {
                var user = db.Users.Where(u => u.UserName == username).FirstOrDefault();
                if (user != null)
                {
                    return user.Roles.Select(r => r.Name).ToArray();
                }
                return new string[] { };
            }
        }

        public bool IsUserInRole(string username, string roleName)
        {
            return GetRolesForUser(username).Any(r => r.Equals(roleName));
        }

        public bool IsUserInRoles(string username, string[] roleNames)
        {
            var roles = GetRolesForUser(username);
            var hasRole = true;

            foreach(var role in roleNames)
            {
                if(!(roles.Any(r => r.Equals(role))))
                {
                    hasRole = false;
                    break;
                }
            }

            return hasRole;
        }
    }
}
