﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class PagingResult
    {
        public int NumberOfPages { get; set; }
    }
}
