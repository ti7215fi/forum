﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Implementations
{
    public class PaginationService
    {
        public PagingResult GetPagingResult<T>(PagingParameter pagingParameter, List<T> list)
        {
            return new PagingResult
            {
                NumberOfPages = (int)Math.Ceiling((double)list.Count() / pagingParameter.PageSize),
            };
        }

    }
}
