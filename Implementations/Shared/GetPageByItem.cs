﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class GetPageByItem
    {
        public int WhichPage { get; set; }
        public IEnumerable<object> ItemsInPage { get; set; }

        public static GetPageByItem Of(int pageNumber, IEnumerable<object> items)
        {
            return new GetPageByItem
            {
                WhichPage = pageNumber,
                ItemsInPage = items
            };
        }
    }
}
