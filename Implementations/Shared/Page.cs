﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class Page
    {
        public static IEnumerable<Forum> Of(IEnumerable<Forum> forums, int take, int skip)
        {
            return forums
                .OrderBy(f => f.Position)
                .Skip(skip)
                .Take(take);
        }

        public static IEnumerable<Thread> Of(IEnumerable<Thread> threads, int take, int skip)
        {
            return threads
                .OrderByDescending(p => p.CreatedAt)
                .Skip(skip)
                .Take(take);
        }

        public static IEnumerable<Post> Of(IEnumerable<Post> posts, int take, int skip)
        {
            return posts
                .OrderByDescending(p => p.CreatedAt)
                .Skip(skip)
                .Take(take);
        }
    }
}
