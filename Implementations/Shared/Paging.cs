﻿using System;

namespace Implementations
{
    public class Paging
    {
        public readonly static double ITEMS_PER_PAGE = 10.0;

        public string   LinkPrevPage        { get; set; }
        public string   LinkNextPage        { get; set; }
        public int      MaxPageCount        { get; set; }
        public int      TotalRecordCount    { get; set; }
        public int      ActivePage          { get; set; }


        public Paging (int totalRecordCount)
        {
            TotalRecordCount = totalRecordCount;
            MaxPageCount = (int)Math.Ceiling(totalRecordCount / ITEMS_PER_PAGE);
            ActivePage = 1;
            if(MaxPageCount == 0)
            {
                MaxPageCount = 1;
            }
        }

    }
}
