﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class PagingParameter
    {
        public int PageNumber   { get; set; }
        public int PageSize     { get; set; } = 10;
    }
}
