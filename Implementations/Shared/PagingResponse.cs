﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class PagingResponse
    {
        public object Body { get; set; }
        public int TotalRecordCount { get; set; }
    }
}
