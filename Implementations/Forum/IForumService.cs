﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public interface IForumService
    {
        ForumListView GetAllForums(string username, int take, int skip);
        ForumEditView GetForumById(int id, string username);
        ForumDetailView GetForumDetailsById(int id, string username, int take, int skip);
        void CreateForum(ForumNewView view, string username);
        void UpdateForum(int id, ForumEditView view);
        void RemoveForum(int id);
        SwitchForumPositionsResponse SwitchForumPosition(SwitchForumPositionsRequest view, string username);
    }
}
