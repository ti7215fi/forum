﻿using Definitions;
using System;

namespace Implementations
{
    public static class ForumTypeService
    {
        public readonly static string[] ForumTypes = {
            ForumType.ANNOUNCEMENT,
            ForumType.DISCUSSION,
            ForumType.PROPOSAL
        };

        public static bool IsValid(string forumType)
        {
            return Array.IndexOf(ForumTypes, forumType) > -1;
        }
    }
}
