﻿using Definitions;
using System.Collections.Generic;
using System.Linq;

namespace Implementations
{
    public static class ForumFilterService
    {
        public static IEnumerable<Forum> FilterByType(IEnumerable<Forum> forums, string type)
        {
            return forums.Where(f => f.Type.Equals(type));
        }

        public static IEnumerable<Forum> FilterByTypes(IEnumerable<Forum> forums, string[] types)
        {
            var filteredForums = new List<Forum>();
            foreach (string type in types)
            {
                var result = FilterByType(forums, type);
                if (result.Count() > 0)
                {
                    filteredForums.AddRange(result);
                }
            }
            return filteredForums;
        }
    }
}
