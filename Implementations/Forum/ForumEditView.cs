﻿using Definitions;

namespace Implementations
{
    public class ForumEditView
    {
        public string Name          { get; set; }
        public string Type          { get; set; }
        public string Description   { get; set; }
        public bool FeatureThreadStatus { get; set; }
        public string RoleReadForum { get; set; }
        public string RoleCreateUpdateThread { get; set; }
        public string RoleCreateUpdatePost { get; set; }

        public void AssignToForum(ForumContainer context, Forum forum)
        {
            forum.Name = Name;
            forum.Type = Type;
            forum.Description = Description;
            forum.ForumConfig.FeatureThreadStatus = FeatureThreadStatus;
            forum.ForumConfig.RoleCanCreateEditThread = RoleService.Instance.GetRoleByName(context, RoleCreateUpdateThread);
            forum.ForumConfig.RoleCanRead = RoleService.Instance.GetRoleByName(context, RoleReadForum);
            forum.ForumConfig.RoleCanCreateEditPost = RoleService.Instance.GetRoleByName(context, RoleCreateUpdatePost);
        }

        public static ForumEditView OfForum(Forum forum, string username)
        {
            var view = new ForumEditView
            {
                Name = forum.Name,
                Type = forum.Type,
                Description = forum.Description,
                FeatureThreadStatus = forum.ForumConfig.FeatureThreadStatus,
                RoleReadForum = forum.ForumConfig.RoleCanRead.Name,
                RoleCreateUpdateThread = forum.ForumConfig.RoleCanCreateEditThread.Name,
                RoleCreateUpdatePost = forum.ForumConfig.RoleCanCreateEditPost.Name
            };
            return view;
        }
    }
}
