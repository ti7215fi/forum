﻿using Definitions;
using System.Collections.Generic;
using System.Linq;

namespace Implementations
{
    public class ForumDao : IForumDao
    {
        public ForumDao() { }

        public IEnumerable<Forum> GetForums(ForumContainer context)
        {
            return context.Forums.ToList();
        }

        public Forum GetForum(ForumContainer context, int id)
        {
            return context.Forums.Where(f => f.Id == id).FirstOrDefault();
        }

        public void CreateForum(ForumContainer context, Forum forum)
        {
            context.Forums.Add(forum);
            context.SaveChanges();
        }

        public void UpdateForum(ForumContainer context, Forum updatedForum)
        {
            context.SaveChanges();
        }

        public void RemoveForum(ForumContainer context, Forum forum)
        {
            context.ForumConfigs.Remove(forum.ForumConfig); 
            context.Forums.Remove(forum);
            context.SaveChanges();
        }

    }
}
