﻿using Definitions;
using System;

namespace Implementations
{
    public class ForumNewView
    {
        public string Name                      { get; set; }
        public string Type                      { get; set; }
        public string Description               { get; set; }
        public bool FeatureThreadStatus         { get; set; }
        public string RoleReadForum             { get; set; }
        public string RoleCreateUpdateThread    { get; set; }
        public string RoleCreateUpdatePost      { get; set; }

        public void AssignToForum(ForumContainer context, Forum forum, User creator, int position)
        {
            forum.Name = Name;
            forum.Type = Type;
            forum.Description = Description;
            forum.CreatedBy = creator;
            forum.CreatedAt = DateTime.Now;
            forum.Position = position;
            forum.ForumConfig = new ForumConfig
            {
                FeatureThreadStatus = FeatureThreadStatus,
                RoleCanCreateEditThread = RoleService.Instance.GetRoleByName(context, RoleCreateUpdateThread),
                RoleCanRead = RoleService.Instance.GetRoleByName(context, RoleReadForum),
                RoleCanCreateEditPost = RoleService.Instance.GetRoleByName(context, RoleCreateUpdatePost)
            };
        }
    }
}