﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Implementations
{
    public class ForumService : IForumService
    {
        private readonly IDbContextFactory _DbContextFactory;
        private readonly IForumDao _Dao;

        public ForumService(IDbContextFactory dbContextFactory,IForumDao dao) {
            _DbContextFactory = dbContextFactory;
            _Dao = dao;
        }

        public ForumListView GetAllForums(string username, int take, int skip)
        {
            var context = _DbContextFactory.CreateNewContext();
            var userRoles = UserRoleService.Instance.GetRolesForUser(username);
            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);

            var forums = _Dao
                .GetForums(context)
                .Where(f => Array.IndexOf(userRoles, f.ForumConfig.RoleCanRead.Name) != -1);
            int totalRecordCount = forums.Count();
            var page = Page.Of(forums, take, skip);
            var result = ForumListView.OfForums(context, page, isAdmin, totalRecordCount);

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public ForumEditView GetForumById(int id, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var forum = _Dao.GetForum(context, id);
            if (forum == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(id);
            }
            var result = ForumEditView.OfForum(forum, username);

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public ForumDetailView GetForumDetailsById(int id, string username, int take, int skip)
        {
            var context = _DbContextFactory.CreateNewContext();

            var forum = _Dao.GetForum(context, id);
            if (forum == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(id);
            }
            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);
            var threads = forum.Threads.AsEnumerable();
            IEnumerable<Thread> deletedThreads = null;
            IEnumerable<Thread> archivedThreads = null;
            var deletedThreadCount = 0;
            var archivedThreadCount = 0;

            if (!isAdmin)
            {
                threads = ThreadFilterService.FilterActiveThreads(threads);
                threads = ThreadFilterService.FilterUnlockedThreads(threads);
            } else
            {
                deletedThreads = ThreadFilterService.FilterDeletedThreads(threads);
                deletedThreadCount = deletedThreads.Count();

                archivedThreads = ThreadFilterService.FilterArchivedThreads(threads);
                archivedThreadCount = archivedThreads.Count();

                threads = ThreadFilterService.FilterActiveThreads(threads);
            }
            int totalRecordCount = threads.Count();
            threads = threads.Skip(skip).Take(take);

            var userCanCreateThreads = UserDao.Instance.HasRole(context, username, forum.ForumConfig.RoleCanCreateEditThread.Name);
            var result = ForumDetailView.OfForum(
                forum, 
                threads, 
                username, 
                userCanCreateThreads, 
                totalRecordCount, 
                deletedThreadCount, 
                archivedThreadCount, 
                deletedThreads, 
                archivedThreads
            );

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public void CreateForum(ForumNewView view, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var newForum = new Forum();
            var forum = _Dao.GetForums(context).OrderByDescending( f => f.Position).FirstOrDefault();
            var position = 10;
            if(forum != null)
            {
                position = forum.Position + 10;
            }
            var user = UserDao.Instance.GetUserByUserName(context, username);
            if(user == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }
            view.AssignToForum(context, newForum, user, position);

            _Dao.CreateForum(context, newForum);
            _DbContextFactory.DisposeContext(context);
        }

        public void UpdateForum(int id, ForumEditView view)
        {
            var context = _DbContextFactory.CreateNewContext();

            var forum = _Dao.GetForum(context, id);
            if (forum == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(id);
            }
            view.AssignToForum(context, forum);
            _Dao.UpdateForum(context, forum);
            _DbContextFactory.DisposeContext(context);
        }

        public void RemoveForum(int id)
        {
            var context = _DbContextFactory.CreateNewContext();

            var forum = _Dao.GetForum(context, id);
            if (forum == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(id);
            }
            
            _Dao.RemoveForum(context, forum);
            _DbContextFactory.DisposeContext(context);
        }

        public SwitchForumPositionsResponse SwitchForumPosition(SwitchForumPositionsRequest view, string username)
        {
            var context = _DbContextFactory.CreateNewContext();
            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);

            if (view.ForumAId == null && view.ForumBId == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new InvalidOperationException();
            }

            Forum forumA = null;
            Forum forumB = null;
            Forum forumNewInPage = null;
            int itemsPerPage = (int)Paging.ITEMS_PER_PAGE;

            int skip = view.SortDown ? 
                itemsPerPage * view.CurrentPage :                               
                itemsPerPage * (view.CurrentPage - 1) - 1;

            if (view.ForumAId == null)
            {
                forumNewInPage = forumA = GetForumNewInPage(context, skip);
            } else
            {
                forumA = _Dao.GetForum(context, (int)view.ForumAId);
                if (forumA == null)
                {
                    _DbContextFactory.DisposeContext(context);
                    throw new EntityNotFoundException((int)view.ForumAId);
                }
            }


            if(view.ForumBId == null)
            {
                forumNewInPage = forumB = GetForumNewInPage(context, skip);
            } else
            {
                forumB = _Dao.GetForum(context, (int)view.ForumBId);
                if (forumB == null)
                {
                    _DbContextFactory.DisposeContext(context);
                    throw new EntityNotFoundException((int)view.ForumBId);
                }
            }

            var forumAPos = forumA.Position;
            forumA.Position = forumB.Position;
            forumB.Position = forumAPos;

            _Dao.UpdateForum(context, null);
            var result = SwitchForumPositionsResponse.OfForum(forumNewInPage, isAdmin);

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        private Forum GetForumNewInPage(ForumContainer context, int skip)
        {
            return _Dao
                .GetForums(context)
                .OrderBy(f => f.Position)
                .Skip(skip)
                .First();
        }

    }
}
