﻿using Definitions;
using System.Collections.Generic;

namespace Implementations
{
    public class ForumListView
    {
        public List<ForumListEntryView> Forums  { get; set; }
        public Paging                   Paging  { get; set; }

        public static ForumListView OfForums(ForumContainer context, IEnumerable<Forum> forums, bool isAdmin, int totalRecordCount)
        {
            var view = new ForumListView
            {
                Forums = new List<ForumListEntryView>(),
                Paging = new Paging(totalRecordCount)
            };
            foreach (var forum in forums)
            {
                view.Forums.Add(ForumListEntryView.OfForum(forum, isAdmin));
            }
            return view;
        }
    }
}