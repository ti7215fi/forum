﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class SwitchForumPositionsResponse
    {
        public ForumListEntryView ForumNewInPage { get; set; }

        public static SwitchForumPositionsResponse OfForum(Forum forum, bool isAdmin)
        {
            var view = new SwitchForumPositionsResponse
            {
                ForumNewInPage = forum != null ? ForumListEntryView.OfForum(forum, isAdmin) : null
            };
            return view;
        }
    }
}
