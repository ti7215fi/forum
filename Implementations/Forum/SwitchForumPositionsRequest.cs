﻿namespace Implementations
{
    public class SwitchForumPositionsRequest
    {
        public int?  ForumAId       { get; set; }
        public int?  ForumBId       { get; set; }
        public bool  SortDown       { get; set; }
        public int   CurrentPage    { get; set; }
    }   
}
