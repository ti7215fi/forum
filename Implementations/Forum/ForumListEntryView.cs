﻿using Definitions;
using System;
using System.Linq;

namespace Implementations
{
    public class ForumListEntryView
    {
        public int Id               { get; set; }
        public string Name          { get; set; }
        public string Type          { get; set; }
        public string LastActivity  { get; set; }
        public int NumberOfThreads  { get; set; }
        public string Creator       { get; set; }

        public static ForumListEntryView OfForum(Forum forum, bool isAdmin)
        {
            var activeThreads = ThreadFilterService
                .FilterActiveThreads(forum.Threads)
                .ToList();
            var numberOfThreads = activeThreads.Count;

            if (!isAdmin)
            {
                var unlockedThreads = ThreadFilterService.FilterUnlockedThreads(activeThreads);
                numberOfThreads = unlockedThreads.Count();
            }

            var view = new ForumListEntryView
            {
                Id = forum.Id,
                Name = forum.Name,
                Type = forum.Type,
                LastActivity = DateTime.Now.ToLongDateString(),
                NumberOfThreads = numberOfThreads,
                Creator = forum.CreatedBy.FirstName + " " + forum.CreatedBy.LastName
            };
            return view;
        }
    }
}