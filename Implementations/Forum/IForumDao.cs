﻿using Definitions;
using System.Collections.Generic;

namespace Implementations
{
    public interface IForumDao
    {
        IEnumerable<Forum> GetForums(ForumContainer context);
        Forum GetForum(ForumContainer context, int id);
        void CreateForum(ForumContainer context, Forum forum);
        void UpdateForum(ForumContainer context, Forum updatedForum);
        void RemoveForum(ForumContainer context, Forum forum);
    }
}
