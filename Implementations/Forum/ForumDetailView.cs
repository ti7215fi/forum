﻿using Definitions;
using System.Collections.Generic;

namespace Implementations
{
    public class ForumDetailView
    {
        public string               Type                { get; set; }
        public string               Name                { get; set; }
        public string               Description         { get; set; }
        public bool                 UserCanCreateThread { get; set; }
        public ThreadListView       ThreadList          { get; set; }
        public ThreadListView       DeletedThreadList   { get; set; }
        public ThreadListView       ArchivedThreadList  { get; set; }
        public Paging               Paging              { get; set; }

        public static ForumDetailView OfForum(
            Forum forum, 
            IEnumerable<Thread> threads,
            string userNameOfCurrentUser, 
            bool userCanCreateThread, 
            int totalRecordCountThreads,
            int totalRecordCountThreadsDeleted,
            int totalRecordCountThreadsArchived,
            IEnumerable<Thread> deletedThreads = null,
            IEnumerable<Thread> archivedThreads = null
            )
        {
            var view = new ForumDetailView
            {
                Name = forum.Name,
                Type = forum.Type,
                Description = forum.Description,
                UserCanCreateThread = userCanCreateThread,
                ThreadList = ThreadListView.OfThreads(threads, userNameOfCurrentUser, forum.ForumConfig.FeatureThreadStatus, totalRecordCountThreads),
                DeletedThreadList = deletedThreads != null ? ThreadListView.OfThreads(deletedThreads, userNameOfCurrentUser, forum.ForumConfig.FeatureThreadStatus, totalRecordCountThreadsDeleted) : null,
                ArchivedThreadList = archivedThreads != null ? ThreadListView.OfThreads(archivedThreads, userNameOfCurrentUser, forum.ForumConfig.FeatureThreadStatus, totalRecordCountThreadsArchived) : null,
                Paging = new Paging(totalRecordCountThreads)
            };
            view.ThreadList.Paging = view.Paging;
            return view;
        }
    }
}
