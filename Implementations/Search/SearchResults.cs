﻿using System.Collections.Generic;

namespace Implementations
{
    public class SearchResults
    {
        public string                   SearchTerm  { get; set; }
        public int                      Count       { get; set; }
        public List<ThreadSearchResult> Threads     { get; set; }
        public List<PostSearchResult>   Posts       { get; set; }

        public static SearchResults OfResults(string searchTerm, List<ThreadSearchResult> threads, List<PostSearchResult> posts)
        {
            var view = new SearchResults
            {
                SearchTerm = searchTerm,
                Threads = threads,
                Posts = posts,
                Count = threads.Count + posts.Count
            };
            return view;
        }
    }
}
