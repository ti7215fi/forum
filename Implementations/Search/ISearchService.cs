﻿namespace Implementations
{
    public interface ISearchService
    {
        SearchResults GetSearchResults(string searchTerm, string username);
    }
}
