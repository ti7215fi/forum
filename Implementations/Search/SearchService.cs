﻿using System.Linq;

namespace Implementations
{
    public class SearchService : ISearchService
    {
        private readonly IThreadService _ThreadService;
        private readonly IPostService   _PostService;
        public SearchService(IThreadService threadService, IPostService postService) {
            _ThreadService = threadService;
            _PostService = postService;
        }

        public SearchResults GetSearchResults(string searchTerm, string username)
        {
            var threads = _ThreadService.GetThreadsBySearchTerm(searchTerm, username);
            var posts = _PostService.GetPostsBySearchTerm(searchTerm, username);
            return SearchResults.OfResults(searchTerm, threads, posts);
        }
    }
}
