﻿namespace Implementations
{
    interface ISearchResult
    {
        int Id { get; set; }
        int ForumId { get; set; }
        string Heading { get; set; }
        string Creator { get; set; }
        string Description { get; set; }
        string ForumName { get; set; }
    }
}
