﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class RoleDao : BaseDao
    {
        private static RoleDao _Instance;
        private RoleDao() { }

        public static RoleDao Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new RoleDao();
                }
                return _Instance;
            }
        }

        public Role GetRoleByName(ForumContainer context, string roleName)
        {
            return context.Roles.Where(r => r.Name.Equals(roleName)).FirstOrDefault();
        }
    }
}
