﻿using Definitions;

namespace Implementations
{
    class RoleService
    {
        private static RoleService _Instance;
        private RoleService() { }

        public static RoleService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new RoleService();
                }
                return _Instance;
            }
        }

        public Role GetRoleByName(ForumContainer context, string roleName)
        {
            var role = RoleDao.Instance.GetRoleByName(context, roleName);
            if(role == null)
            {
                throw new EntityNotFoundException();
            }
            return role;
        } 

    }
}
