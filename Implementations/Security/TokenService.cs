﻿using System;
using System.Text;

namespace Implementations
{
    public class TokenService
    {
        private static TokenService _Instance;
        private TokenService() { }

        public static TokenService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new TokenService();
                }
                return _Instance;
            }
        }

        public string CreateToken(string userName, string password)
        {
            var token = userName + ":" + password;
            var tokenInBytes = Encoding.UTF8.GetBytes(token);
            return Convert.ToBase64String(tokenInBytes);
        }

    }
}
