﻿namespace Implementations
{
    public class LoginResponse
    {
        public string   Token   { get; set; }
        public bool     IsAdmin { get; set; }

        public static LoginResponse OfUser(string token, bool isAdmin)
        {
            var view = new LoginResponse
            {
                Token = token,
                IsAdmin = isAdmin
            };
            return view;
        }
    }
}
