﻿namespace Implementations
{
    public interface IAuthService
    {
        LoginResponse ValidateUser(LoginRequest request);
    }
}
