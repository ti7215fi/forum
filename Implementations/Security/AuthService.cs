﻿using Definitions;
using System;
using System.Linq;
using System.Text;

namespace Implementations
{
    public class AuthService : IAuthService
    {
        private readonly IAuthDao _Dao;

        public AuthService(IAuthDao dao) {
            _Dao = dao;
        }

        public LoginResponse ValidateUser(LoginRequest request)
        {
            var username = Encoding.UTF8.GetString(Convert.FromBase64String(request.Username));
            var password = Encoding.UTF8.GetString(Convert.FromBase64String(request.Password));
            var user = _Dao.GetUserByUserNameAndPassword(username, password);
            if(user == null)
            {
                throw new EntityNotFoundException();
            }
            var token = TokenService.Instance.CreateToken(username, password);
            var isAdmin = user.Roles.Where(r => r.Name.Equals(UserRole.ADMIN)).FirstOrDefault() != null;

            return LoginResponse.OfUser(token, isAdmin);
        }

        public static bool ValidateUser(string username, string password, string[] roles) 
        {
            using (ForumContainer context = new ForumContainer())
            {
                bool isAuthorized = context.Users.FirstOrDefault(u =>
                    u.UserName.Equals(username) &&
                    u.Password.Equals(password)
                ) != null;
                bool hasRoles = UserRoleService.Instance.IsUserInRoles(username, roles);
                return isAuthorized && hasRoles;
            }
        }

    }
}
