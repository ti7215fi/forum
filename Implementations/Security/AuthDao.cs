﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public class AuthDao : BaseDao, IAuthDao
    {
        public AuthDao() { }

        public User GetUserByUserNameAndPassword(string username, string password)
        {
            return _Container.Users.FirstOrDefault(u =>
                    u.UserName.Equals(username) &&
                    u.Password.Equals(password)
            );
        }


    }
}
