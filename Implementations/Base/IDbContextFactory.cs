﻿using Definitions;

namespace Implementations
{
    public interface IDbContextFactory
    {
        ForumContainer CreateNewContext();
        void DisposeContext(ForumContainer context);
    }
}
