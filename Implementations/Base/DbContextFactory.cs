﻿using Definitions;

namespace Implementations
{
    public class DbContextFactory : IDbContextFactory
    {
        public ForumContainer CreateNewContext()
        {
            return new ForumContainer();
        }

        public void DisposeContext(ForumContainer context)
        {
            if (context != null)
            {
                context.Dispose();
            }
        }
    }
}
