﻿using Definitions;
using System.Linq;
using System;
using System.Collections.Generic;

namespace Implementations
{
    public class PostService : IPostService
    {
        private readonly IDbContextFactory _DbContextFactory;
        private readonly IPostDao _Dao;
        private readonly IThreadDao _ThreadDao;

        public PostService(IDbContextFactory dbContextFactory, IPostDao dao, IThreadDao threadDao) {
            _DbContextFactory = dbContextFactory;
            _Dao = dao;
            _ThreadDao = threadDao;
        }

        public List<PostSearchResult> GetPostsBySearchTerm(string searchTerm, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var userRoles = UserRoleService.Instance.GetRolesForUser(username);
            var posts = _Dao
                .GetPostsBySearchTerm(context, searchTerm)
                .Where(p => Array.IndexOf(userRoles, p.Thread.Forum.ForumConfig.RoleCanRead.Name) != -1);
            var views = new List<PostSearchResult>();
            foreach(Post post in posts)
            {
                var view = PostSearchResult.OfPost(post);
                views.Add(view);
            }

            _DbContextFactory.DisposeContext(context);
            return views;
        }

        public PostListView GetPostsByThreadId(int threadId, string username, int take, int skip)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _ThreadDao.GetThread(context, threadId);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(threadId);
            }
            var totalRecordCount = thread.Posts.Count();
            var posts = Page.Of(thread.Posts, take, skip);
            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);
            var result = PostListView.OfPosts(posts, username, isAdmin, totalRecordCount);

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public GetPageByItem SearchForPostInPages(IEnumerable<Post> posts, int postIdToSearch)
        {
            var take = 10;
            var skip = 0;

            var paging = new Paging(posts.Count());
            int pageNumber = -1;
            Post foundedPost = null;
            IEnumerable<Post> result = null;

            for (var i = 0; i < paging.MaxPageCount; ++i)
            {
                if (foundedPost == null)
                {
                    result = Page.Of(posts, take, skip);
                    foundedPost = result.Where(p => p.Id == postIdToSearch).FirstOrDefault();
                    skip += take;
                }
                else
                {
                    break;
                }
            }

            if(foundedPost == null)
            {
                throw new EntityNotFoundException(postIdToSearch);
            }

            pageNumber = skip / (int)Paging.ITEMS_PER_PAGE;

            return GetPageByItem.Of(pageNumber, result);
        }

        public PostCreateResponse CreatePost(PostNewView view, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var creator = UserDao.Instance.GetUserByUserName(context, username);
            if(creator == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }

            var thread = _ThreadDao.GetThread(context, view.ThreadId);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(view.ThreadId);
            }

            var isAuthorized = UserDao.Instance.HasRole(context, username, thread.Forum.ForumConfig.RoleCanCreateEditPost.Name);
            if(!isAuthorized)
            {
                _DbContextFactory.DisposeContext(context);
                throw new UnauthorizedException();
            }

            var newPost = new Post();
            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);
            view.AssignToPost(newPost, creator, thread);
            _Dao.CreatePost(context, newPost);

            var listEntry = PostListEntryView.OfPost(newPost, username, isAdmin);
            var paging = new Paging(thread.Posts.Count());
            var result = new PostCreateResponse
            {
                MaxPageCount = paging.MaxPageCount,
                Post = PostListEntryView.OfPost(newPost, username, isAdmin)
            };

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public PostListEntryView UpdatePost(int id, PostEditView view, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var post = _Dao.GetPost(context, id);
            if (post == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(id);
            }

            var isAuthorized = UserDao.Instance.HasRole(context, username, post.Thread.Forum.ForumConfig.RoleCanCreateEditPost.Name);
            if (!isAuthorized)
            {
                _DbContextFactory.DisposeContext(context);
                throw new UnauthorizedException();
            }
            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);
            view.AssignToPost(post);
            _Dao.UpdatePost(context, post);
            var result = PostListEntryView.OfPost(post, username, isAdmin);

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public void RemovePost(int id, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var post = _Dao.GetPost(context, id);
            if (post == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(id);
            }

            var isAuthorized = UserDao.Instance.HasRole(context, username, post.Thread.Forum.ForumConfig.RoleCanCreateEditPost.Name);
            if (!isAuthorized)
            {
                _DbContextFactory.DisposeContext(context);
                throw new UnauthorizedException();
            }

            _Dao.RemovePost(context, post);
            _DbContextFactory.DisposeContext(context);
        }
    }
}
