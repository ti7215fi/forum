﻿using Definitions;

namespace Implementations
{
    public class PostEditView
    {
        public string ContentPlain { get; set; }
        public string ContentHtml { get; set; }

        public void AssignToPost(Post post)
        {
            post.DescriptionFormatted = ContentHtml;
            post.DescriptionPlain = ContentPlain;
        }
    }
}