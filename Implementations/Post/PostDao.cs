﻿using Definitions;
using System.Collections.Generic;
using System.Linq;

namespace Implementations
{
    public class PostDao : BaseDao, IPostDao
    {
        public PostDao() { }

        public Post GetPost(ForumContainer context, int id)
        {
            return context.Posts.Where(p => p.Id == id).FirstOrDefault();
        }

        public IEnumerable<Post> GetPostsBySearchTerm(ForumContainer context, string searchTerm)
        {
            return context.Posts.AsEnumerable().Where(p => Extensions.CaseInsensitiveContains(p.DescriptionPlain, searchTerm));
        }

        public void CreatePost(ForumContainer context, Post post)
        {
            context.Posts.Add(post);
            context.SaveChanges();
        }

        public void UpdatePost(ForumContainer context, Post updatedPost)
        {
            context.SaveChanges();
        }

        public void RemovePost(ForumContainer context, Post post)
        {
            context.Posts.Remove(post);
            context.SaveChanges();
        }
    }
}
