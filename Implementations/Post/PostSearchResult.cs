﻿using Definitions;

namespace Implementations
{
    public class PostSearchResult : ISearchResult
    {
        public int Id               { get; set; }
        public int ForumId          { get; set; }
        public int ThreadId         { get; set; }
        public string Heading       { get; set; }
        public string Creator       { get; set; }
        public string Description   { get; set; }
        public string ForumName     { get; set; }

        public static PostSearchResult OfPost(Post post)
        {
            var view = new PostSearchResult
            {
                Id = post.Id,
                ForumId = post.Thread.Forum.Id,
                ThreadId = post.Thread.Id,
                Heading = "Beitrag",
                Creator = post.CreatedBy.FirstName + ' ' + post.CreatedBy.LastName,
                ForumName = post.Thread.Forum.Name,
                Description = post.DescriptionPlain,
            };
            return view;
        }
    }
}
