﻿using Definitions;
using System.Collections.Generic;

namespace Implementations
{
    public class PostListView
    {
        public List<PostListEntryView>  Posts   { get; set; }
        public Paging                   Paging  { get; set; }

        public static PostListView OfPosts(IEnumerable<Post> posts, string userNameOfCurrentUser, bool isAdmin, int totalRecordCount)
        {
            var view = new PostListView
            {
                Posts = new List<PostListEntryView>(),
                Paging = new Paging(totalRecordCount),
            };
            foreach (Post post in posts)
            {
                view.Posts.Add(PostListEntryView.OfPost(post, userNameOfCurrentUser, isAdmin));
            }
            return view;
        }
    }
}