﻿namespace Implementations
{
    public class PostCreateResponse
    {
        public PostListEntryView    Post            { get; set; }
        public int                  MaxPageCount    { get; set; }
    }
}
