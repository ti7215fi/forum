﻿using Definitions;
using System.Collections.Generic;

namespace Implementations
{
    public interface IPostDao
    {
        Post GetPost(ForumContainer context, int id);
        IEnumerable<Post> GetPostsBySearchTerm(ForumContainer context, string searchTerm);
        void CreatePost(ForumContainer context, Post post);
        void UpdatePost(ForumContainer context, Post updatedPost);
        void RemovePost(ForumContainer context, Post post);
    }
}
