﻿using Definitions;
using System;

namespace Implementations
{
    public class PostNewView
    {
        public int      ThreadId             { get; set; }
        public string   ContentPlain         { get; set; }
        public string   ContentHtml     { get; set; }

        public void AssignToPost(Post post, User creator, Thread thread)
        {
            post.DescriptionFormatted = ContentHtml;
            post.DescriptionPlain = ContentPlain;
            post.CreatedAt = DateTime.Now;
            post.CreatedBy = creator;
            post.Thread = thread;
        }

    }
}