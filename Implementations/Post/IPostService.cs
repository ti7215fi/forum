﻿using Definitions;
using System.Collections.Generic;

namespace Implementations
{
    public interface IPostService
    {
        List<PostSearchResult> GetPostsBySearchTerm(string searchTerm, string username);
        GetPageByItem SearchForPostInPages(IEnumerable<Post> posts, int postIdToSearch);
        PostListView GetPostsByThreadId(int threadId, string username, int take, int skip);
        PostCreateResponse CreatePost(PostNewView view, string username);
        PostListEntryView UpdatePost(int id, PostEditView view, string username);
        void RemovePost(int id, string username);
    }
}
