﻿using Definitions;
using System;

namespace Implementations
{
    public class PostListEntryView
    {
        public int Id               { get; set; }
        public string Creator       { get; set; }
        public string CreatedAt     { get; set; }
        public string UpdatedAt     { get; set; }
        public string Content       { get; set; }
        public bool CanEdit         { get; set; }

        public static PostListEntryView OfPost(Post post, string userNameOfCurrentUser, bool isAdmin)
        {
            var view = new PostListEntryView
            {
                Id = post.Id,
                Creator = post.CreatedBy.FirstName + " " + post.CreatedBy.LastName,
                CreatedAt = post.CreatedAt.ToShortDateString(),
                UpdatedAt = DateTime.Now.ToShortDateString(), // ToDo
                Content = post.DescriptionFormatted,
                CanEdit = isAdmin || post.CreatedBy.UserName.Equals(userNameOfCurrentUser)
            };
            return view;
        }
    }
}