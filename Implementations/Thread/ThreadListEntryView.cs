﻿using Definitions;
using System;

namespace Implementations
{
    public class ThreadListEntryView
    {
        public int      Id              { get; set; }
        public bool     Unlocked        { get; set; }
        public bool     IsDeleted       { get; set; }
        public bool     HasCreated      { get; set; }
        public string   Title           { get; set; }
        public string   Description     { get; set; }
        public string   Creator         { get; set; }
        public string   LastActivity    { get; set; }
        public string   Status          { get; set; }

        public static ThreadListEntryView OfThread(Thread thread, string userNameOfCurrentUser)
        {
            var view = new ThreadListEntryView
            {
                Id = thread.Id,
                Unlocked = thread.Unlocked,
                Title = thread.Title,
                Description = thread.DescriptionFormatted,
                Creator = thread.CreatedBy.FirstName + " " + thread.CreatedBy.LastName,
                LastActivity = DateTime.Now.ToShortDateString(), // ToDo
                Status = thread.Status,
                IsDeleted = thread.DeletedAt != null,
                HasCreated = thread.CreatedBy.UserName.Equals(userNameOfCurrentUser)
            };
            return view;
        }
    }
}