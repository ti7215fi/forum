﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Implementations
{
    public class ThreadDao : IThreadDao
    {
        public ThreadDao() { }

        public Thread GetThread(ForumContainer context, int id)
        {
            return context.Threads.Where(t => t.Id == id).FirstOrDefault();
        }

        public IEnumerable<Thread> GetThreadsByForumId(ForumContainer context, int forumId)
        {
            return context.Threads.AsEnumerable().Where(t => t.Forum.Id == forumId);
        }

        public IEnumerable<Thread> GetThreadsBySearchTerm(ForumContainer context, string searchTerm)
        {
            return context.Threads.AsEnumerable().Where(t => 
                Extensions.CaseInsensitiveContains(t.DescriptionPlain, searchTerm) || 
                Extensions.CaseInsensitiveContains(t.Title, searchTerm)
            );
        }

        public void CreateThread(ForumContainer context, Thread thread)
        {
            context.Threads.Add(thread);
            context.SaveChanges();
        }

        public void UpdateThread(ForumContainer context, Thread updatedThread)
        {
            context.SaveChanges();
        }

        public void RemoveThread(ForumContainer context, Thread thread)
        {
            context.Threads.Remove(thread);
            context.SaveChanges();
        }

    }
}
