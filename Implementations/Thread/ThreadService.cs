﻿using Definitions;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Implementations
{
    public class ThreadService : IThreadService
    {
        private readonly IDbContextFactory _DbContextFactory;
        private readonly IThreadDao _Dao;
        private readonly IForumDao  _ForumDao;
        private readonly IPostService _PostService;

        public ThreadService(IDbContextFactory dbContextFactory, IThreadDao dao, IForumDao forumDao, IPostService postService) {
            _DbContextFactory = dbContextFactory;
            _Dao = dao;
            _ForumDao = forumDao;
            _PostService = postService;
        }

        public ThreadEditResponse GetThreadById(int id)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _Dao.GetThread(context, id);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(id);
            }
            var result = ThreadEditResponse.OfThread(thread);

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public List<ThreadSearchResult> GetThreadsBySearchTerm(string searchTerm, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var userRoles = UserRoleService.Instance.GetRolesForUser(username);
            var threads = _Dao.
                GetThreadsBySearchTerm(context,searchTerm)
                .Where(t => Array.IndexOf(userRoles, t.Forum.ForumConfig.RoleCanRead.Name) != -1);

            var resultViews = new List<ThreadSearchResult>();
            foreach(Thread thread in threads)
            {
                var view = ThreadSearchResult.OfThread(thread);
                resultViews.Add(view);
            }

            _DbContextFactory.DisposeContext(context);
            return resultViews;
        }

        public ThreadListView GetThreadsByForumId(int forumId, string username, string which, int take, int skip)
        {
            var context = _DbContextFactory.CreateNewContext();

            var forum = _ForumDao.GetForum(context, forumId);
            if(forum == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }
            var threads = ThreadFilterService.Filter(forum.Threads, which);
            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);
            if(which.Equals(ThreadType.ACTIVE) && !isAdmin)
            {
                threads = ThreadFilterService.FilterUnlockedThreads(threads);
            }
            var totalRecordCount = threads.Count();

            threads = Page.Of(threads, take, skip);

            var forumHasStatusFeature = forum.ForumConfig.FeatureThreadStatus;
            var result = ThreadListView.OfThreads(threads, username, forumHasStatusFeature, totalRecordCount);

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public ThreadDetailView GetThreadDetailsById(int id, string username, int take, int skip, int? postToHighlightId = null)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _Dao.GetThread(context, id);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException(id);
            }
            var totalRecordCount = thread.Posts.Count();

            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);
            var userCanCreatePost = UserDao.Instance.HasRole(context, username, thread.Forum.ForumConfig.RoleCanCreateEditPost.Name);
            var activePage = 1;
            IEnumerable<Post> posts = null;
            GetPageByItem searchResult = null;

            if (postToHighlightId == null)
            {
                posts = Page.Of(thread.Posts, take, skip);
            } else
            {
                searchResult = _PostService.SearchForPostInPages(thread.Posts, (int)postToHighlightId);
                posts = (IEnumerable<Post>)searchResult.ItemsInPage;
                activePage = searchResult.WhichPage;
            }

            var result = ThreadDetailView.OfThread(thread, posts.ToList(), username, isAdmin, userCanCreatePost, totalRecordCount);
            result.Paging.ActivePage = activePage;

            _DbContextFactory.DisposeContext(context);
            return result;
        }

        public void CreateThread(ThreadNewView view, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var newThread = new Thread();

            var creator = UserDao.Instance.GetUserByUserName(context, username);
            if (creator == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }

            var forum = _ForumDao.GetForum(context, view.ForumId);
            if (forum == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }

            var isAuthorized = UserDao.Instance.HasRole(context, username, forum.ForumConfig.RoleCanCreateEditThread.Name);
            if (!isAuthorized)
            {
                _DbContextFactory.DisposeContext(context);
                throw new UnauthorizedException();
            }

            view.AssignToThread(newThread, creator, forum);

            _Dao.CreateThread(context, newThread);
            _DbContextFactory.DisposeContext(context);
        }

        public void UpdateThread(int id, ThreadEditRequest view, string username)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _Dao.GetThread(context, id);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }

            var isAuthorized = UserDao.Instance.HasRole(context, username, thread.Forum.ForumConfig.RoleCanCreateEditThread.Name);
            if (!isAuthorized)
            {
                _DbContextFactory.DisposeContext(context);
                throw new UnauthorizedException();
            }
            view.AssignToThread(thread);

            _Dao.UpdateThread(context, thread);
            _DbContextFactory.DisposeContext(context);
        }

        public void DeleteThread(int id, string username, bool hardDelete)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _Dao.GetThread(context, id);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }

            var isAuthorized = UserDao.Instance.HasRole(context, username, thread.Forum.ForumConfig.RoleCanCreateEditThread.Name);
            if (!isAuthorized)
            {
                _DbContextFactory.DisposeContext(context);
                throw new UnauthorizedException();
            }

            var isAdmin = UserDao.Instance.HasRole(context, username, UserRole.ADMIN);
            // only admins can hard delete threads
            if(!hardDelete)
            {
                thread.DeletedAt = DateTime.Now;
                _Dao.UpdateThread(context, thread);
            } else if(isAdmin)
            {
                _Dao.RemoveThread(context, thread);
            } else
            {
                _DbContextFactory.DisposeContext(context);
                throw new UnauthorizedException();
            }

            _DbContextFactory.DisposeContext(context);
        }

        public void UnlockThread(int id, bool unlock)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _Dao.GetThread(context, id);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }
            if (thread.Unlocked != unlock)
            {
                thread.Unlocked = unlock;
                _Dao.UpdateThread(context, thread);
            }

            _DbContextFactory.DisposeContext(context);
        }

        public void RecoverThread(int id)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _Dao.GetThread(context, id);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }
            thread.DeletedAt = null;
            thread.ArchivedAt = null;

            _Dao.UpdateThread(context, thread);
            _DbContextFactory.DisposeContext(context);
        }

        public void ArchiveThread(int id)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _Dao.GetThread(context, id);
            if (thread == null)
            {
                _DbContextFactory.DisposeContext(context);
                throw new EntityNotFoundException();
            }
            thread.ArchivedAt = DateTime.Now;

            _Dao.UpdateThread(context, thread);
            _DbContextFactory.DisposeContext(context);
        }

        public void ChangeThreadStatus(int id, string status)
        {
            var context = _DbContextFactory.CreateNewContext();

            var thread = _Dao.GetThread(context, id);
            if (thread == null)
            {
                throw new EntityNotFoundException();
            }
            if (!ThreadStatusValidator.IsValid(status))
            {
                // ToDo
            }
            thread.Status = status;

            _Dao.UpdateThread(context, thread);
            _DbContextFactory.DisposeContext(context);
        }
    }
}
