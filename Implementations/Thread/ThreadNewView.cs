﻿using Definitions;
using System;

namespace Implementations
{
    public class ThreadNewView
    {
        public int      ForumId             { get; set; }
        public bool     Unlock              { get; set; }
        public string   Title               { get; set; }
        public string   DescriptionPlain    { get; set; }
        public string   DescriptionHtml     { get; set; }

        public void AssignToThread(Thread thread, User creator, Forum forum)
        {
            thread.Title = Title;
            thread.DescriptionFormatted = DescriptionHtml;
            thread.DescriptionPlain = DescriptionPlain;
            thread.CreatedAt = DateTime.Now;
            thread.CreatedBy = creator;
            thread.Unlocked = Unlock;
            thread.Forum = forum;
        }
    }
}