﻿using Definitions;
using System.Collections.Generic;

namespace Implementations
{
    public class ThreadListView
    {
        public bool                         ForumHasStatusFeature   { get; set; }
        public List<ThreadListEntryView>    Threads                 { get; set; }
        public Paging                       Paging                  { get; set; }

        public static ThreadListView OfThreads(IEnumerable<Thread> threads, string usernameOfCurrentUser, bool forumHasStatusFeature, int totalRecordCount)
        {
            var view = new ThreadListView
            {
                ForumHasStatusFeature = forumHasStatusFeature,
                Threads = new List<ThreadListEntryView>(),
                Paging = new Paging(totalRecordCount)
            };
            foreach (Thread thread in threads)
            {
                view.Threads.Add(ThreadListEntryView.OfThread(thread, usernameOfCurrentUser));
            }
            return view;
        }
    }
}