﻿using Definitions;

namespace Implementations
{
    public class ThreadEditRequest
    {
        public string   Title               { get; set; }
        public string   DescriptionPlain    { get; set; }
        public string   DescriptionHtml     { get; set; }
        public bool     Unlock              { get; set; }

        public void AssignToThread(Thread thread)
        {
            thread.Title = Title;
            thread.DescriptionFormatted = DescriptionHtml;
            thread.DescriptionPlain = DescriptionPlain;
            thread.Unlocked = Unlock;
        }
    }
}