﻿namespace Implementations
{
    public static class ThreadType
    {
        public const string ACTIVE    = "ACTIVE";
        public const string DELETED   = "DELETED";
        public const string ARCHIVED  = "ARCHIVED";
    }
}
