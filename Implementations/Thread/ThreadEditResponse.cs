﻿using Definitions;

namespace Implementations
{
    public class ThreadEditResponse
    {
        public string   ForumName       { get; set; }
        public string   Title           { get; set; }
        public string   Description     { get; set; }
        public int      ForumId         { get; set; }
        public bool     Unlock          { get; set; }

        public static ThreadEditResponse OfThread(Thread thread)
        {
            var view = new ThreadEditResponse
            {
                ForumId = thread.Forum.Id,
                ForumName = thread.Forum.Name,
                Title = thread.Title,
                Description = thread.DescriptionFormatted,
                Unlock = thread.Unlocked
            };
            return view;
        }
    }
}