﻿using Definitions;
using System.Collections.Generic;
using System.Linq;

namespace Implementations
{
    public static class ThreadFilterService
    {
        public static IEnumerable<Thread> FilterUnlockedThreads(IEnumerable<Thread> threads)
        {
            return threads.Where(t => t.Unlocked == true).OrderBy(t => t.CreatedAt);
        }

        public static IEnumerable<Thread> FilterActiveThreads(IEnumerable<Thread> threads)
        {
            return threads
                .Where(t => t.DeletedAt == null && t.ArchivedAt == null)
                .OrderBy(t => t.CreatedAt);
        }

        public static IEnumerable<Thread> FilterDeletedThreads(IEnumerable<Thread> threads)
        {
            return threads.Where(t => t.DeletedAt != null).OrderBy(t => t.DeletedAt);
        }

        public static IEnumerable<Thread> FilterArchivedThreads(IEnumerable<Thread> threads)
        {
            return threads.Where(t => t.ArchivedAt != null).OrderBy(t => t.ArchivedAt);
        }

        public static IEnumerable<Thread> Filter(IEnumerable<Thread> threads, string type)
        {
            switch (type)
            {
                case ThreadType.ACTIVE:
                    return FilterActiveThreads(threads);
                case ThreadType.DELETED:
                    return FilterDeletedThreads(threads);
                case ThreadType.ARCHIVED:
                    return FilterArchivedThreads(threads);
                default:
                    return null;
            }
        }
    }
}
