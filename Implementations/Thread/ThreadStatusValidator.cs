﻿using Definitions;
using System;

namespace Implementations
{
    public static class ThreadStatusValidator
    {
        public readonly static string[] ForumTypes = {
            ThreadStatus.REJECTED,
            ThreadStatus.REVIEW,
            ThreadStatus.ACCEPTED
        };

        public static bool IsValid(string forumType)
        {
            return Array.IndexOf(ForumTypes, forumType) > -1;
        }
    }
}
