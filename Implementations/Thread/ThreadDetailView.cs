﻿using Definitions;
using System;
using System.Collections.Generic;

namespace Implementations
{
    public class ThreadDetailView
    {
        public int          ForumId                 { get; set; }
        public string       ForumName               { get; set; }
        public string       ForumType               { get; set; }
        public string       CreatedAt               { get; set; }
        public string       UpdatedAt               { get; set; }
        public string       Title                   { get; set; }
        public string       Description             { get; set; }
        public string       Status                  { get; set; }
        public string       Creator                 { get; set; }
        public bool         UserCanCreatePost       { get; set; }
        public bool         ForumHasStatusFeature   { get; set; }
        public PostListView PostList                { get; set; }
        public Paging       Paging                  { get; set; }

        public static ThreadDetailView OfThread(Thread thread, List<Post> posts,string usernameOfCurrentUser, bool isAdmin, bool userCanCreatePost, int totalRecordCountPosts)  
        {
            var view = new ThreadDetailView
            {
                ForumId = thread.Forum.Id,
                ForumName = thread.Forum.Name,
                ForumType = thread.Forum.Type,
                CreatedAt = thread.CreatedAt.ToShortDateString(),
                UpdatedAt = DateTime.Now.ToShortDateString(), // ToDo
                Title = thread.Title,
                Description = thread.DescriptionFormatted,
                Status = thread.Status,
                Creator = thread.CreatedBy.FirstName + " " + thread.CreatedBy.LastName,
                UserCanCreatePost = userCanCreatePost,
                ForumHasStatusFeature = thread.Forum.ForumConfig.FeatureThreadStatus,
                PostList = PostListView.OfPosts(posts, usernameOfCurrentUser, isAdmin, totalRecordCountPosts),
                Paging = new Paging(totalRecordCountPosts)
            };
            view.PostList.Paging = view.Paging;
            return view;
        }
    }
}