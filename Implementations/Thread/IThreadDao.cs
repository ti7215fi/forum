﻿using Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public interface IThreadDao
    {
        Thread GetThread(ForumContainer context, int id);
        IEnumerable<Thread> GetThreadsByForumId(ForumContainer context, int forumId);
        IEnumerable<Thread> GetThreadsBySearchTerm(ForumContainer context, string searchTerm);
        void CreateThread(ForumContainer context, Thread thread);
        void UpdateThread(ForumContainer context, Thread updatedThread);
        void RemoveThread(ForumContainer context, Thread thread);
    }
}
