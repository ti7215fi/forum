﻿using Definitions;

namespace Implementations
{
    public class ThreadSearchResult : ISearchResult
    {
        public int      Id          { get; set; }
        public int      ForumId     { get; set; }
        public string   Heading     { get; set; }
        public string   Creator     { get; set; }
        public string   Description { get; set; }
        public string   ForumName   { get; set; }

        public static ThreadSearchResult OfThread(Thread thread)
        {
            var view = new ThreadSearchResult
            {
                Id = thread.Id,
                ForumId = thread.Forum.Id,
                Heading = "Thema: " + thread.Title,
                Creator = thread.CreatedBy.FirstName + ' ' + thread.CreatedBy.LastName,
                ForumName = thread.Forum.Name,
                Description = thread.DescriptionPlain,
            };
            return view;
        }
    }
}
