﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implementations
{
    public interface IThreadService
    {
        ThreadEditResponse GetThreadById(int id);
        List<ThreadSearchResult> GetThreadsBySearchTerm(string searchTerm, string username);
        ThreadListView GetThreadsByForumId(int forumId, string username, string which, int take, int skip);
        ThreadDetailView GetThreadDetailsById(int id, string username, int take, int skip, int? postToHighlightId = null);
        void CreateThread(ThreadNewView view, string username);
        void UpdateThread(int id, ThreadEditRequest view, string username);
        void DeleteThread(int id, string username, bool hardDelete);
        void UnlockThread(int id, bool unlock);
        void RecoverThread(int id);
        void ArchiveThread(int id);
        void ChangeThreadStatus(int id, string status);
    }
}
