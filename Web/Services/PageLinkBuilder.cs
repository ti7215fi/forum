﻿using Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Web.Services
{
    public class PageLinkBuilder
    {
        public Uri NextPage { get; private set; }
        public Uri PrevPage { get; private set; }

        public PageLinkBuilder(UrlHelper urlHelper, object routeValues, int numberOfPage, int pageSize, long totalRecordCount, string routeName = "DefaultApi")
        {
            // Determine total number of pages
            var pageCount = totalRecordCount > 0
                ? (int)Math.Ceiling(totalRecordCount / (double)pageSize)
                : 0;

            if (numberOfPage > 1)
            {
                PrevPage = new Uri(urlHelper.Link(routeName ,new HttpRouteValueDictionary(routeValues)
            {
                {"pageNumber", numberOfPage - 1},
                {"pageSize", pageSize}
            }));
            }
            if (numberOfPage < pageCount)
            {
                NextPage = new Uri(urlHelper.Link(routeName, new HttpRouteValueDictionary(routeValues)
            {
                {"pageNumber", numberOfPage + 1},
                {"pageSize", pageSize}
            }));
            }
        }
    }
}