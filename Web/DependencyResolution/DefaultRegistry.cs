// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Web.DependencyResolution {
    using Implementations;
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using Web.Areas.HelpPage.Controllers;

    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            Scan(
                scan => {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
            // fix that WebApi.Helpage works with StructureMap 
            For<HelpController>().Use(ctx => new HelpController());

            For<IDbContextFactory>().Use<DbContextFactory>();

            For<IUserService>().Use<UserService>();

            For<IForumService>().Use<ForumService>();
            For<IForumDao>().Use<ForumDao>();

            For<IThreadService>().Use<ThreadService>();
            For<IThreadDao>().Use<ThreadDao>();

            For<IPostService>().Use<PostService>();
            For<IPostDao>().Use<PostDao>();

            For<ISearchService>().Use<SearchService>();

            For<IAuthService>().Use<AuthService>();
            For<IAuthDao>().Use<AuthDao>();
        }

        #endregion
    }
}