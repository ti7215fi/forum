﻿using Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Filters;

namespace Web.Controllers
{
    public class SearchController : ApiController
    {
        private readonly ISearchService _Service;

        public SearchController(ISearchService service)
        {
            _Service = service;
        }

        [HttpGet]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/search", Name = "Search")]
        public HttpResponseMessage Search(string searchTerm)
        {
            return Request.CreateResponse(HttpStatusCode.OK, _Service.GetSearchResults(searchTerm, User.Identity.Name));
        }

    }
}