﻿using Implementations;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Filters;
using Web.Services;

namespace Web.Controllers
{
    public class ForumController : ApiController
    {

        private readonly IForumService _Service;
        private readonly IThreadService _ThreadService;

        public ForumController(IForumService service, IThreadService threadService)
        {
            _Service = service;
            _ThreadService = threadService;
        }

        [HttpGet]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/forums", Name = "GetAllForums")]
        public HttpResponseMessage GetAllForums([FromUri]PagingParameter pagingParameter)
        {
            int skip = pagingParameter.PageSize * (pagingParameter.PageNumber - 1);
            int take = pagingParameter.PageSize;
            var body = _Service.GetAllForums(User.Identity.Name, take, skip);

            // Get the page links
            var linkBuilder = new PageLinkBuilder(Url, null, pagingParameter.PageNumber, pagingParameter.PageSize, body.Paging.TotalRecordCount, "GetAllForums");

            if (linkBuilder.PrevPage != null)
                body.Paging.LinkPrevPage = linkBuilder.PrevPage.ToString();
            if (linkBuilder.NextPage != null)
                body.Paging.LinkNextPage = linkBuilder.NextPage.ToString();

            return Request.CreateResponse(HttpStatusCode.OK, body);
        }

        [HttpPost]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/forums", Name = "CreateForum")]
        public HttpResponseMessage CreateForum([FromBody]ForumNewView view)
        {
            try
            {
                _Service.CreateForum(view, User.Identity.Name);
                return new HttpResponseMessage(HttpStatusCode.Created);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/forums/{id}", Name = "GetForumById")]
        public HttpResponseMessage GetForumById(int id)
        {
            try
            {
                var body = _Service.GetForumById(id, User.Identity.Name);
                return Request.CreateResponse(HttpStatusCode.OK, body);
            }
            catch(EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpPut]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/forums/{id}", Name = "UpdateForum")]
        public HttpResponseMessage UpdateForum(int id, [FromBody]ForumEditView view)
        {
            try
            {
                _Service.UpdateForum(id, view);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpDelete]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/forums/{id}", Name = "DeleteForum")]
        public HttpResponseMessage DeleteForum(int id)
        {
            try
            {
                _Service.RemoveForum(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }


        [HttpGet]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/forums/{id}/details", Name = "GetForumDetailsById")]
        public HttpResponseMessage GetForumDetailsById(int id, [FromUri]PagingParameter pagingParameter)
        {
            try
            {
                int skip = pagingParameter.PageSize * (pagingParameter.PageNumber - 1);
                int take = pagingParameter.PageSize;
                var body = _Service.GetForumDetailsById(id, User.Identity.Name, take, skip);

                // Get the page links
                var linkBuilder = new PageLinkBuilder(Url, new { which = ThreadType.ACTIVE }, pagingParameter.PageNumber, pagingParameter.PageSize, body.Paging.TotalRecordCount, "GetForumThreads");

                if (linkBuilder.PrevPage != null)
                    body.Paging.LinkPrevPage = linkBuilder.PrevPage.ToString();
                if (linkBuilder.NextPage != null)
                    body.Paging.LinkNextPage = linkBuilder.NextPage.ToString();

                if (body.ArchivedThreadList != null)
                {
                    linkBuilder = new PageLinkBuilder(Url, new { which = ThreadType.ARCHIVED }, pagingParameter.PageNumber, pagingParameter.PageSize, body.ArchivedThreadList.Paging.TotalRecordCount, "GetForumThreads");

                    if (linkBuilder.PrevPage != null)
                        body.ArchivedThreadList.Paging.LinkPrevPage = linkBuilder.PrevPage.ToString();
                    if (linkBuilder.NextPage != null)
                        body.ArchivedThreadList.Paging.LinkNextPage = linkBuilder.NextPage.ToString();
                }

                if (body.DeletedThreadList != null)
                {
                    linkBuilder = new PageLinkBuilder(Url, new { which = ThreadType.DELETED }, pagingParameter.PageNumber, pagingParameter.PageSize, body.DeletedThreadList.Paging.TotalRecordCount, "GetForumThreads");

                    if (linkBuilder.PrevPage != null)
                        body.DeletedThreadList.Paging.LinkPrevPage = linkBuilder.PrevPage.ToString();
                    if (linkBuilder.NextPage != null)
                        body.DeletedThreadList.Paging.LinkNextPage = linkBuilder.NextPage.ToString();
                }

                return Request.CreateResponse(HttpStatusCode.OK, body);
            }
            catch(EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/forums/{id}/threads", Name = "GetForumThreads")]
        public HttpResponseMessage GetForumThreads(int id, string which, [FromUri]PagingParameter pagingParameter)
        {
            try
            {
                int skip = pagingParameter.PageSize * (pagingParameter.PageNumber - 1);
                int take = pagingParameter.PageSize;
                var body = _ThreadService.GetThreadsByForumId(id, User.Identity.Name, which, take, skip);

                // Get the page links
                var linkBuilder = new PageLinkBuilder(Url, new { which = which }, pagingParameter.PageNumber, pagingParameter.PageSize, body.Paging.TotalRecordCount, "GetForumThreads");

                if (linkBuilder.PrevPage != null)
                    body.Paging.LinkPrevPage = linkBuilder.PrevPage.ToString();
                if (linkBuilder.NextPage != null)
                    body.Paging.LinkNextPage = linkBuilder.NextPage.ToString();

                return Request.CreateResponse(HttpStatusCode.OK, body);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpPatch]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/forums/switch-positions", Name = "SwitchForumPositions")]
        public HttpResponseMessage SwitchForumPositions([FromBody]SwitchForumPositionsRequest view)
        {
            try
            {
                var body = _Service.SwitchForumPosition(view, User.Identity.Name);
                return Request.CreateResponse(HttpStatusCode.OK, body);
            }
            catch(EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }
    }
}