﻿using Implementations;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Filters;
using Web.Services;

namespace Web.Controllers
{
    public class ThreadController : ApiController
    {
        private readonly IThreadService _Service;
        private readonly IPostService _PostService;

        public ThreadController(IThreadService service, IPostService postService)
        {
            _Service = service;
            _PostService = postService;
        }

        [HttpPost]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/threads", Name = "CreateThread")]
        public HttpResponseMessage CreateThread([FromBody]ThreadNewView view)
        {
            try
            {
                _Service.CreateThread(view, User.Identity.Name);
                return new HttpResponseMessage(HttpStatusCode.Created);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (UnauthorizedException ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        [HttpGet]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/threads/{id}", Name = "GetThreadById")]
        public HttpResponseMessage GetThreadById(int id)
        {
            try
            {
                var body = _Service.GetThreadById(id);
                return Request.CreateResponse(HttpStatusCode.OK, body);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpPut]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/threads/{id}", Name = "UpdateThread")]
        public HttpResponseMessage UpdateThread(int id, [FromBody]ThreadEditRequest view)
        {
            try
            {
                _Service.UpdateThread(id, view, User.Identity.Name);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (UnauthorizedException ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        [HttpDelete]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/threads/{id}", Name = "DeleteThread")]
        public HttpResponseMessage DeleteThread(int id, bool hardDelete = false)
        {
            try
            {
                _Service.DeleteThread(id, User.Identity.Name, hardDelete);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (UnauthorizedException ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        [HttpGet]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/threads/{id}/details", Name = "GetThreadDetailsById")]
        public HttpResponseMessage GetThreadDetailsById(int id, [FromUri]PagingParameter pagingParameter, int? postToHighlight = null)
        {
            try
            {
                int skip = pagingParameter.PageSize * (pagingParameter.PageNumber - 1);
                int take = pagingParameter.PageSize;
                var body = _Service.GetThreadDetailsById(id, User.Identity.Name, take, skip, postToHighlight);
                var pageNumber = pagingParameter.PageNumber;

                if (postToHighlight != null)
                {
                    pageNumber = body.Paging.ActivePage;
                }

                // Get the page links
                var linkBuilder = new PageLinkBuilder(Url, null, pageNumber, pagingParameter.PageSize, body.Paging.TotalRecordCount, "GetThreadPosts");

                if (linkBuilder.PrevPage != null)
                    body.Paging.LinkPrevPage = linkBuilder.PrevPage.ToString();
                if (linkBuilder.NextPage != null)
                    body.Paging.LinkNextPage = linkBuilder.NextPage.ToString();

                return Request.CreateResponse(HttpStatusCode.OK, body);
            }
            catch(EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpGet]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/threads/{id}/posts", Name = "GetThreadPosts")]
        public HttpResponseMessage GetThreadPosts(int id, [FromUri]PagingParameter pagingParameter)
        {
            try
            {
                int skip = pagingParameter.PageSize * (pagingParameter.PageNumber - 1);
                int take = pagingParameter.PageSize;
                var body = _PostService.GetPostsByThreadId(id, User.Identity.Name, take, skip);

                // Get the page links
                var linkBuilder = new PageLinkBuilder(Url, null, pagingParameter.PageNumber, pagingParameter.PageSize, body.Paging.TotalRecordCount, "GetThreadPosts");

                if (linkBuilder.PrevPage != null)
                    body.Paging.LinkPrevPage = linkBuilder.PrevPage.ToString();
                if (linkBuilder.NextPage != null)
                    body.Paging.LinkNextPage = linkBuilder.NextPage.ToString();

                return Request.CreateResponse(HttpStatusCode.OK, body);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpPatch]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/threads/{id}/unlock", Name = "UnlockThread")]
        public HttpResponseMessage UnlockThread(int id, bool unlock)
        {
            try
            {
                _Service.UnlockThread(id, unlock);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpPatch]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/threads/{id}/recover", Name = "RecoverThread")]
        public HttpResponseMessage RecoverThread(int id)
        {
            try
            {
                _Service.RecoverThread(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpPatch]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/threads/{id}/archive", Name = "ArchiveThread")]
        public HttpResponseMessage ArchiveThread(int id)
        {
            try
            {
                _Service.ArchiveThread(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

        [HttpPatch]
        [AuthorizeUser(Roles = "ADMIN")]
        [Route(template: "api/threads/{id}/update-status", Name = "UpdateStatus")]
        public HttpResponseMessage UpdateStatus(int id, string status)
        {
            try
            {
                _Service.ChangeThreadStatus(id, status);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }
    }
}