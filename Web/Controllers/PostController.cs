﻿using Implementations;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Filters;

namespace Web.Controllers
{
    public class PostController : ApiController
    {
        private readonly IPostService _Service;

        public PostController(IPostService service)
        {
            _Service = service;
        }


        [HttpPost]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/posts", Name = "CreatePost")]
        public HttpResponseMessage CreatePost([FromBody]PostNewView view)
        {
            try
            {
                var body = _Service.CreatePost(view, User.Identity.Name);
                return Request.CreateResponse(HttpStatusCode.Created, body);
            } 
            catch(EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (UnauthorizedException ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        [HttpPut]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/posts/{id}", Name = "UpdatePost")]
        public HttpResponseMessage UpdatePost(int id, [FromBody]PostEditView view)
        {
            try
            {
                var body = _Service.UpdatePost(id, view, User.Identity.Name);
                return Request.CreateResponse(HttpStatusCode.OK, body);
            }
            catch(EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (UnauthorizedException ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        [HttpDelete]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/posts/{id}", Name = "DeletePost")]
        public HttpResponseMessage DeletePost(int id)
        {
            try
            {
                _Service.RemovePost(id, User.Identity.Name);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch(EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (UnauthorizedException ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }
    }
}