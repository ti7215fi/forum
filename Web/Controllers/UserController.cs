﻿using Implementations;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.Filters;

namespace Web.Controllers
{
    public class UserController : ApiController
    {

        private readonly IUserService _Service;

        public UserController(IUserService service)
        {
            _Service = service;
        }

        [HttpGet]
        [AuthorizeUser(Roles = "DEFAULT_USER")]
        [Route(template: "api/users/isAdmin", Name = "IsAdmin")]
        public HttpResponseMessage IsAdmin()
        {
            var body = _Service.IsAdmin(User.Identity.Name);
            return Request.CreateResponse(HttpStatusCode.OK, body);
        }
    }
}