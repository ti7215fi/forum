﻿using Implementations;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Web.Controllers
{
    public class AuthController : ApiController
    {
        private readonly IAuthService _Service;

        public AuthController(IAuthService service)
        {
            _Service = service;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route(template: "api/auth/login", Name = "Login")]
        public HttpResponseMessage Login([FromBody]LoginRequest request)
        {
            try
            {
                var response = _Service.ValidateUser(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (EntityNotFoundException ex)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }

    }
}
