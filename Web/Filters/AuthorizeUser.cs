﻿using Implementations;
using System;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Web.DependencyResolution;

namespace Web.Filters
{

    public class AuthorizeUser : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            if(actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            } else
            {
                string authToken = actionContext.Request.Headers.Authorization.Parameter;
                string decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(authToken));
                string username = decodedToken.Substring(0, decodedToken.IndexOf(":"));
                string password = decodedToken.Substring(decodedToken.IndexOf(":") + 1);
                string[] roles = Roles.Split(new[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

                bool isValid = AuthService.ValidateUser(username, password, roles);

                if (isValid)
                {
                    var identity = new GenericIdentity(username);
                    var principal = new GenericPrincipal(identity, null);
                    SetPrincipal(principal);
                    return true;
                } else
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
                }
            }

            return base.IsAuthorized(actionContext);
        }

        private static void SetPrincipal(IPrincipal principal)
        {
            System.Threading.Thread.CurrentPrincipal = principal;
            if(HttpContext.Current != null)
            {
                HttpContext.Current.User = principal;
            }
        }
    }
}