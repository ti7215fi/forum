import { Component } from '@angular/core';

/* tslint:disable:component-selector */
@Component({
  selector: 'div[app-forums]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
/* tslint:enable:component-selector */
