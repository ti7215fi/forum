import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SecurityModule } from './security/security.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { ForumModule } from './forum/forum.module';
import { ThreadModule } from './thread/thread.module';
import { PostModule } from './post/post.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TokenInterceptor } from './security/token.interceptor';
import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgHttpLoaderModule,
    AppRoutingModule,
    SecurityModule,
    ForumModule,
    ThreadModule,
    PostModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
