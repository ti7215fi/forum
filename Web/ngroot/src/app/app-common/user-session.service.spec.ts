import { TestBed, inject } from '@angular/core/testing';

import { UserSessionService } from './user-session.service';

describe('UserSessionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserSessionService]
    });
  });

  xit('should be created', inject([UserSessionService], (service: UserSessionService) => {
    expect(service).toBeTruthy();
  }));
});
