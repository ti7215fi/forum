import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from './api.service';
import { DeleteDataModalComponent } from './delete-data-modal/delete-data-modal.component';
import { UserSessionService } from './user-session.service';
import { PaginationComponent } from './pagination/pagination.component';
import { UserService } from './user.service';
import { TrustHtmlPipe } from './trust-html.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    DeleteDataModalComponent,
    PaginationComponent,
    TrustHtmlPipe
  ],
  declarations: [DeleteDataModalComponent, PaginationComponent, TrustHtmlPipe],
  providers: [ApiService, UserSessionService, UserService]
})
export class AppCommonModule {
}
