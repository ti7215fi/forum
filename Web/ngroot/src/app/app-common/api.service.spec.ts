import { TestBed, inject } from '@angular/core/testing';

import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';
import { TokenService } from '../security/token.service';

class HttpClientStub {
  get() {}
  post() {}
  put() {}
  patch() {}
  delete() {}
}

class TokenServiceStub {
  retrieveToken() {}
}

describe('ApiService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ApiService,
        { provide: HttpClient, useClass: HttpClientStub },
        { provide: TokenService, useClass: TokenServiceStub }
      ]
    });
  });

  it('should be created', inject([ApiService], (service: ApiService) => {
    expect(service).toBeTruthy();
  }));
});
