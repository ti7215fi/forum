declare interface EntityToDelete {
    id: number;
    name: string;
}

declare interface UserSession {
    isAdmin: boolean;
    forumName: string;
}

declare interface Paging {
    linkPrevPage: string;
    linkNextPage: string;
    maxPageCount: number;
    totalRecordCount: number;
    activePage: number;
}

declare interface PagingParameter {
    pageNumber: number;
    pageSize: number;
}

declare interface DeleteItemResponseStrategy {
    supports(wasLastItemInPage: boolean, onlyOnePage: boolean): boolean;
    handleResponse(...args): any;
}