import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserService {

  private readonly API_ROOT_URL: string = 'users';

  constructor(
    private apiService: ApiService
  ) { }

  isUserAdmin(): Observable<boolean> {
    return this.apiService.authGet(`${this.API_ROOT_URL}/isAdmin`);
  }

}
