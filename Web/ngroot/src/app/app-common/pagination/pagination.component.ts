import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../api.service';
import { CPagingParameter } from '../app-common-definitions';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  @Output() pagingByNumberEvent = new EventEmitter<PagingParameter>();
  @Output() pagignPrevResponseEvent = new EventEmitter<any>();
  @Output() pagingNextResponseEvent = new EventEmitter<any>();

  @Input() paging: Paging;

  pagingNumbers: number[];
  currentPage: number;

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.currentPage = this.paging.activePage || 1;
    this.updatePaging(this.paging);
  }

  pagingByNumber(number: number) {
    const pagingParams = new CPagingParameter();
    pagingParams.pageNumber = number;
    this.pagingByNumberEvent.emit(pagingParams);
  }

  handlePagingByNumberResponse(pageNumber: number) {
    this.currentPage = pageNumber;
  }

  pagingPrev(decreasePagingNumbers: boolean = false) {
    this.apiService.authGet(this.paging.linkPrevPage).subscribe(
      response => {
        this.pagignPrevResponseEvent.emit(response);
        this.currentPage--;
        if (decreasePagingNumbers) {
          this.pagingNumbers.pop();
        }
      },
      e => console.error(e)
    );
  }

  pagingNext(increasePagingNumbers: boolean = false) {
    this.apiService.authGet(this.paging.linkNextPage).subscribe(
      response => {
        this.pagingNextResponseEvent.emit(response);
        this.currentPage++;
        if (increasePagingNumbers) {
          this.pagingNumbers.push(this.currentPage);
        }
      },
      e => console.error(e)
    );
  }

  pageEqualsCurrentPage(pageNumber: number) {
    return this.currentPage === pageNumber;
  }

  noPrevPage() {
    return !this.paging.linkPrevPage;
  }

  noNextPage() {
    return !this.paging.linkNextPage;
  }

  updatePaging(paging: Paging) {
    this.paging = paging;
    this.pagingNumbers = Array(paging.maxPageCount).fill(1, 0, paging.maxPageCount).map((x, i) => i + 1);
  }

}
