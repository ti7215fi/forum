import { Observable } from 'rxjs/Observable';

export class CEntityToDelete implements EntityToDelete {
    id: number;
    name: string;
}

export interface IApiService {
    query(...args): Observable<any>;
    get(id: number): Observable<any>;
    create(data: any): Observable<any>;
    update(id: number, data: any): Observable<any>;
    delete(id: number): Observable<any>;
}

export const ItemsPerPage = 10;

export class CPagingParameter implements PagingParameter {
    pageNumber: number;
    pageSize: number;

    constructor() {
        this.pageNumber = 1;
        this.pageSize = ItemsPerPage;
    }
}
