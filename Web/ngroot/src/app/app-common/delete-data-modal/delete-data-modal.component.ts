import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core/src/metadata/directives';
import { CEntityToDelete } from '../app-common-definitions';

@Component({
  selector: 'app-delete-data-modal',
  templateUrl: './delete-data-modal.component.html',
  styleUrls: ['./delete-data-modal.component.css']
})
export class DeleteDataModalComponent implements OnInit {

  entity: EntityToDelete;
  visible: boolean;

  visibleAnimate: boolean;

  constructor() { }

  ngOnInit() {
    this.visible = false;
    this.visibleAnimate = false;
    this.entity = new CEntityToDelete();
  }

  showErrorMessage(entityToDelete: EntityToDelete): void {
    this.entity = entityToDelete;
    this.visible = true;
    setTimeout(() => this.visibleAnimate = true, 100);
  }

  hide(): void {
    this.visibleAnimate = false;
    setTimeout(() => this.visible = false, 300);
  }

  onContainerClicked(event: MouseEvent): void {
    if ((<HTMLElement>event.target).classList.contains('modal')) {
      this.hide();
    }
  }
}
