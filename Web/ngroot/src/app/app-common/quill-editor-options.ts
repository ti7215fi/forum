const QUILL_EDITOR_TOOLBAR = [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons

    [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent

    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],

    ['link']                                           // link
];

export const QUILL_EDITOR_MODULES = {
    toolbar: QUILL_EDITOR_TOOLBAR
};
