import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UserService } from './user.service';

@Injectable()
export class UserSessionService {

  private _forumName: BehaviorSubject<string>;
  private _isAdmin: BehaviorSubject<boolean>;

  private readonly SSK_FORUM_NAME = 'forum_name';

  private busy: Boolean;

  constructor(private userService: UserService) {
    this._forumName = new BehaviorSubject<string>(null);
    this._isAdmin = new BehaviorSubject<boolean>(null);
    this.busy = false;
  }

  get forumName(): string {
    let forumName = this._forumName.value;
    if (forumName === null) {
      forumName = sessionStorage.getItem(this.SSK_FORUM_NAME);
    }
    return forumName;
  }

  set forumName(forumName: string) {
    sessionStorage.setItem(this.SSK_FORUM_NAME, forumName);
    this._forumName.next(forumName);
  }

  get isAdmin(): boolean {
    const isAdmin = this._isAdmin.value;
    if (isAdmin === null && !this.busy) {
      this.busy = true;
      const stub = this.userService.isUserAdmin().subscribe(response => {
        this.isAdmin = response;
        this.busy = false;
      });
    }
    return this._isAdmin.value;
  }

  set isAdmin(isAdmin: boolean) {
    this._isAdmin.next(isAdmin);
  }

}
