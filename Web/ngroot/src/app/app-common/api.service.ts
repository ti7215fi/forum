import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { TokenService } from '../security/token.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ApiService {

  static readonly SERVER_PROTOCOL: string = 'http';
  static readonly SERVER_IP_ADDRESS: string = 'localhost';
  static readonly SERVER_PORT: string = '28903';

  static readonly API_ROOT_URL: string = '/api';

  constructor(
    private http: HttpClient,
    private tokenService: TokenService
  ) { }

  authGet(url: string, options?): Observable<any> {
    return this.http.get(this.getUrl(url), options);
  }

  authPost(url: string, data: any, options?): Observable<any> {
    return this.http.post(this.getUrl(url), data, options);
  }

  authPut(url: string, data: any, options?): Observable<any> {
    return this.http.put(this.getUrl(url), data, options);
  }

  authPatch(url: string, data?: any, options?): Observable<any> {
    return this.http.patch(this.getUrl(url), data, options);
  }

  authDelete(url: string, options?): Observable<any> {
    return this.http.delete(this.getUrl(url), options);
  }

  private getUrl(url: string): string {
    const rootUrl = this.serverAddress() + ApiService.API_ROOT_URL;
    if (url.includes(rootUrl)) {
      return url;
    }
    return `${rootUrl}/${url}`;
  }

  private serverAddress(): string {
    return `${ApiService.SERVER_PROTOCOL}://` +
          `${ApiService.SERVER_IP_ADDRESS}:` +
          `${ApiService.SERVER_PORT}`;
  }

  private getHeaders(): HttpHeaders {
    return new HttpHeaders(
      {
        Authorization: `Basic ${this.tokenService.retrieveToken()}`
      }
    );
  }

}
