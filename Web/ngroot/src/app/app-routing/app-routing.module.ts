import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../security/login/login.component';
import { ForumListComponent } from '../forum/forum-list/forum-list.component';
import { ThreadListComponent } from '../thread/thread-list/thread-list.component';
import { ForumDetailComponent } from '../forum/forum-detail/forum-detail.component';
import { PostListComponent } from '../post/post-list/post-list.component';
import { AuthGuard } from '../security/auth-guard';
import { ForumService } from '../forum/forum.service';
import { ForumListResolver } from '../forum/forum-list/forum-list.resolver';
import { ForumDetailResolver } from '../forum/forum-detail/forum-detail.resolver';
import { ThreadDetailComponent } from '../thread/thread-detail/thread-detail.component';
import { ThreadDetailResolver } from '../thread/thread-detail/thread-detail.resolver';
import { ForumCreateComponent } from '../forum/forum-create/forum-create.component';
import { ThreadCreateComponent } from '../thread/thread-create/thread-create.component';
import { ForumEditComponent } from '../forum/forum-edit/forum-edit.component';
import { ThreadEditComponent } from '../thread/thread-edit/thread-edit.component';
import { PostCreateComponent } from '../post/post-create/post-create.component';
import { ForumEditResolver } from '../forum/forum-edit/forum-edit.resolver';
import { ForumComponent } from '../forum/forum/forum.component';
import { ThreadComponent } from '../thread/thread/thread.component';
import { ThreadEditResolver } from '../thread/thread-edit/thread-edit.resolver';
import { SearchResultComponent } from '../search/search-result/search-result.component';
import { SearchResultResolver } from '../search/search-result/search-result.resolver';
import { ForumThreadsResolver } from '../forum/forum-detail/forum-threads.resolver';
import { ForumThreadsDeletedResolver } from '../forum/forum-detail/forum-threads-deleted.resolver';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'forums',
    component: ForumListComponent,
    canActivate: [AuthGuard],
    resolve: {
      forumList: ForumListResolver
    }
  },
  {
    path: 'forum-create',
    component: ForumCreateComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'forum/:forumId',
    component: ForumComponent,
    canActivate: [AuthGuard],
    children: [
      // forum
      {
        path: '',
        component: ForumDetailComponent,
        canActivate: [AuthGuard],
        resolve: {
          forumDetails: ForumDetailResolver
        },
      },
      {
        path: 'edit',
        component: ForumEditComponent,
        canActivate: [AuthGuard],
        resolve: {
          forum: ForumEditResolver
        }
      },
      // thread
      {
        path: 'thread-create',
        component: ThreadCreateComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'thread/:threadId',
        component: ThreadComponent,
        children: [
          {
            path: '',
            component: ThreadDetailComponent,
            canActivate: [AuthGuard],
            resolve: {
              threadDetails: ThreadDetailResolver
            },
          },
          {
            path: 'edit',
            component: ThreadEditComponent,
            canActivate: [AuthGuard],
            resolve: {
              thread: ThreadEditResolver
            },
          }
        ]
      },
    ]
  },
  {
    path: 'post-create',
    component: PostCreateComponent,
  },
  {
    path: 'search-result/:searchTerm',
    component: SearchResultComponent,
    canActivate: [AuthGuard],
    resolve: {
      searchResults: SearchResultResolver
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    RouterModule
  ],

  exports: [
    RouterModule
  ],

  declarations: []
})
export class AppRoutingModule { }
