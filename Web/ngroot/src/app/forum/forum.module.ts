import { NgModule } from '@angular/core';
import { ForumListComponent } from './forum-list/forum-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { ThreadModule } from '../thread/thread.module';
import { ForumDetailComponent } from './forum-detail/forum-detail.component';
import { AppRoutingModule } from '../app-routing/app-routing.module';
import { RouterModule } from '@angular/router';
import { ForumService } from './forum.service';
import { ForumListResolver } from './forum-list/forum-list.resolver';
import { AppCommonModule } from '../app-common/app-common.module';
import { ForumDetailResolver } from './forum-detail/forum-detail.resolver';
import { ForumCreateComponent } from './forum-create/forum-create.component';
import { ForumEditComponent } from './forum-edit/forum-edit.component';
import { QuillModule } from 'ngx-quill';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForumEditResolver } from './forum-edit/forum-edit.resolver';
import { ForumComponent } from './forum/forum.component';
import { TranslateForumTypePipe } from './translate-forum-type.pipe';
import { SearchModule } from '../search/search.module';
import { ForumThreadsResolver } from './forum-detail/forum-threads.resolver';
import { ForumThreadsDeletedResolver } from './forum-detail/forum-threads-deleted.resolver';

@NgModule({
  imports: [
    RouterModule,
    AppRoutingModule,
    AppCommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ThreadModule,
    QuillModule,
    SearchModule
  ],

  declarations: [
    ForumListComponent,
    ForumDetailComponent,
    ForumCreateComponent,
    ForumEditComponent,
    ForumComponent,
    TranslateForumTypePipe,
  ],
  providers: [
    ForumService,
    ForumListResolver,
    ForumDetailResolver,
    ForumEditResolver,
    ForumThreadsResolver,
    ForumThreadsDeletedResolver
  ]
})
export class ForumModule { }
