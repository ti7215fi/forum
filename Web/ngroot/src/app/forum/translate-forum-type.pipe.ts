import { Pipe, PipeTransform } from '@angular/core';
import { ForumType, FORUM_TYPES } from './forum-types';

@Pipe({
  name: 'translateForumType'
})
export class TranslateForumTypePipe implements PipeTransform {

  transform(key: ForumType): any {
    const forumType = FORUM_TYPES.filter( t => t.key === key )[0];
    return forumType ? forumType.value : 'Nicht definierter Typ';
  }

}
