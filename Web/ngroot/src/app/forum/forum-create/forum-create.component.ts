import { Component, OnInit, ViewChild } from '@angular/core';
import { ForumService } from '../forum.service';
import { Router } from '@angular/router';
import { FORUM_TYPES, ForumTypeSelectOption } from '../forum-types';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { CForumNewView } from '../forum-definitions';
import { QuillEditorComponent } from 'ngx-quill';
import { QUILL_EDITOR_MODULES } from '../../app-common/quill-editor-options';

@Component({
  selector: 'app-forum-create',
  templateUrl: './forum-create.component.html',
  styleUrls: ['./forum-create.component.css']
})
export class ForumCreateComponent implements OnInit {

  forum: ForumNewView;
  forumTypes: ForumTypeSelectOption[];

  @ViewChild(QuillEditorComponent) editor: QuillEditorComponent;
  editorModules = QUILL_EDITOR_MODULES;

  // Validation
  rForm: FormGroup;
  name: FormControl;
  type: FormControl;
  description: FormControl;
  titleAlert = 'Dies ist ein Pflichfeld!';

  constructor(
    private router: Router,
    private forumService: ForumService
  ) { }

  ngOnInit() {
    this.initData();
    this.createFormControls();
    this.createForm();
  }

  createForum() {
    this.forumService.create(this.forum).subscribe(
      (response) => this.router.navigate(['/forums']),
      e => console.error(e)
    );
  }

  private initData() {
    this.forumTypes = FORUM_TYPES;
    this.forum = new CForumNewView();
  }

  private createFormControls() {
    this.name = new FormControl('', Validators.required);
    this.type = new FormControl('', Validators.required);
    this.description = new FormControl('', Validators.required);
  }

  private createForm() {
    this.rForm = new FormGroup({
      name: this.name,
      type: this.type,
      description: this.description
    });
  }

}
