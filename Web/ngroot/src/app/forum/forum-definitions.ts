export class CSwitchForumPositionsRequest implements SwitchForumPositionsRequest {
    forumAId: number;
    forumBId: number;
    sortDown: boolean;
    currentPage: number;

    constructor(forumAId: number, forumBId: number, sortDown: boolean, currentPage: number) {
        this.forumAId = forumAId;
        this.forumBId = forumBId;
        this.sortDown = sortDown;
        this.currentPage = currentPage;
    }
}

export class CForumNewView implements ForumNewView {
    name: string;
    type: string;
    description: string;
    featureThreadStatus: boolean;
    roleReadForum: UserRole;
    roleCreateUpdateThread: UserRole;
    roleCreateUpdatePost: UserRole;

    constructor() {
        this.roleReadForum = 'ADMIN';
        this.roleCreateUpdatePost = 'ADMIN';
        this.roleCreateUpdateThread = 'ADMIN';
    }
}
