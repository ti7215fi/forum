import { Injectable } from '@angular/core';
import { ApiService } from '../app-common/api.service';
import { Observable } from 'rxjs/Observable';
import { IApiService } from '../app-common/app-common-definitions';

@Injectable()
export class ForumService implements IApiService {

  private readonly API_ROOT_URL: string = 'forums';

  constructor(
    private apiService: ApiService
  ) { }

  query(pagingParams: PagingParameter): Observable<ForumListView> {
    return this.apiService.authGet(this.API_ROOT_URL + '?pageNumber=' + pagingParams.pageNumber + '&pageSize=' + pagingParams.pageSize);
  }

  get(id) {
    return this.apiService.authGet(this.API_ROOT_URL + '/' + id);
  }

  getForumDetails(id, pagingParameter: PagingParameter) {
    return this.apiService.authGet(`${this.API_ROOT_URL}/${id}/details` +
    `?pageNumber=${pagingParameter.pageNumber}` +
    `&pageSize=${pagingParameter.pageSize}`);
  }

  getForumThreads(id: number, which: ThreadType, pagingParameter: PagingParameter): Observable<ThreadListView> {
    return this.apiService.authGet(`${this.API_ROOT_URL}/${id}/threads` +
    `?which=${which}` +
    `&pageNumber=${pagingParameter.pageNumber}` +
    `&pageSize=${pagingParameter.pageSize}`);
  }

  create(forum: ForumNewView) {
    return this.apiService.authPost(this.API_ROOT_URL, forum);
  }

  update(id, forum: ForumEditView) {
    return this.apiService.authPut(this.API_ROOT_URL + '/' + id, forum);
  }

  delete(id) {
    return this.apiService.authDelete(this.API_ROOT_URL + '/' + id);
  }

  switchForumPosition(view: SwitchForumPositionsRequest): Observable<SwitchForumPositionsResponse> {
    return this.apiService.authPatch(this.API_ROOT_URL + '/switch-positions', view);
  }
}
