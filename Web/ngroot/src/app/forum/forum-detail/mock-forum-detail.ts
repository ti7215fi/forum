const THREADS: ThreadListEntryView[] = [
    // tslint:disable-next-line:max-line-length
    { id: 1, unlocked: true, title: 'Coolere Farben', description: 'Die Farben unserer Firma sind ein bisschen dunkel, dass sollten wir ändern.', creator: 'Sebastian Metermann', lastActivity: '17. 01. 2018', status: 'Wird umgesetzt', hasCreated: true, isDeleted: false },
    // tslint:disable-next-line:max-line-length
    { id: 2, unlocked: true, title: 'Mehr Urlaub', description: 'Ich brauche mehr Urlaub, damit ich besser entspannen kann.', creator: 'Susi Sorglos', lastActivity: '17. 01. 2018', status: 'Wird nicht umgesetzt', hasCreated: true, isDeleted: true },
    // tslint:disable-next-line:max-line-length
    { id: 3, unlocked: false, title: 'Neue Arbeitsmaterialien', description: 'Mein PC läuft immer noch unter Windows 7. Diese Situation kann ich nicht mehr tragen und meine Programme lassen sich auch nicht mehr updaten.', creator: 'Hinz Kunz', lastActivity: '17. 01. 2018', status: 'In Arbeit', hasCreated: false, isDeleted: false },
    // tslint:disable-next-line:max-line-length
    { id: 4, unlocked: false, title: 'Frische Erbsen', description: 'Erbsen sind gesund. Aber in der Mensa sehen sie selten frisch und grün aus.', creator: 'Susi Sorglos', lastActivity: '17. 01. 2018', status: 'Wird nicht umgesetzt', hasCreated: true, isDeleted: false },
    // tslint:disable-next-line:max-line-length
    { id: 5, unlocked: true, title: 'Neue Philosophie', description: 'Mehr Urlaub, weniger Arbeit!', creator: 'Sebastian Metermann', lastActivity: '17. 01. 2018', status: 'Wird nicht umgesetzt', hasCreated: false, isDeleted: false },
    // tslint:disable-next-line:max-line-length
    { id: 6, unlocked: false, title: 'Angular CLI', description: 'Angular fetzt, so viel ist klar. Wir sollten es überall verwenden.', creator: 'Hinz Kunz', lastActivity: '17. 01. 2018', status: 'Wird umgesetzt', hasCreated: true, isDeleted: false }
];

const THREAD_LIST: ThreadListView = {
    forumHasStatusFeature: false,
    threads: THREADS,
    paging: { linkNextPage: '', linkPrevPage: '', totalRecordCount: THREADS.length, activePage: 1, maxPageCount: 1 }
};

export const FORUM_DETAIL: ForumDetailView = {
    name: 'NTAG Dingens Forum',
    type: 'Vorschlag',
    description: 'Dieses Forum beschäftigt sich mit dem Thema Dingens.',
    threadList: THREAD_LIST,
    deletedThreadList: THREAD_LIST,
    archivedThreadList: THREAD_LIST,
    userCanCreateThread: true,
    paging: { linkNextPage: '', linkPrevPage: '', totalRecordCount: THREADS.length, maxPageCount: 1, activePage: 1 }
};

