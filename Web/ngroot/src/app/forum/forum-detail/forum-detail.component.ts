import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { FORUM_DETAIL } from './mock-forum-detail';
import { UserSessionService } from '../../app-common/user-session.service';
import { ApiService } from '../../app-common/api.service';
import { ForumService } from '../forum.service';
import { CPagingParameter } from '../../app-common/app-common-definitions';
import { ThreadArchivedComponent } from '../../thread/thread-archived/thread-archived.component';
import { ThreadListComponent } from '../../thread/thread-list/thread-list.component';

@Component({
  selector: 'app-forum-detail',
  templateUrl: './forum-detail.component.html',
  styleUrls: ['./forum-detail.component.css']
})
export class ForumDetailComponent implements OnInit {

  @ViewChild(ThreadListComponent) threadListComponent: ThreadListComponent;
  @ViewChild(ThreadArchivedComponent) threadArchivedComponent: ThreadArchivedComponent;

  details;
  view: ForumDetailView;
  forumId: number;

  constructor(
    private route: ActivatedRoute,
    private userSessionService: UserSessionService,
    private apiService: ApiService,
    private forumService: ForumService
  ) { }

  ngOnInit() {
    this.view = this.route.snapshot.data['forumDetails'];
    this.route.params.subscribe(params => {
      this.forumId = params['forumId'];
    });
    this.userSessionService.forumName = this.view.name;
  }

  userIsAdmin(): boolean {
    return this.userSessionService.isAdmin;
  }

  onRecoverThread($event: ThreadListEntryView) {
    const paging = new CPagingParameter();
    paging.pageNumber = this.view.threadList.paging.activePage;
    this.forumService.getForumThreads(this.forumId, 'ACTIVE', paging).subscribe( response => {
      this.view.threadList = response;
      this.threadListComponent.pagination.updatePaging(response.paging);
    }, e => console.error(e));
  }

  onArchiveThreadEvent() {
    const paging = new CPagingParameter();
    paging.pageNumber = this.view.archivedThreadList.paging.activePage;
    this.forumService.getForumThreads(this.forumId, 'ARCHIVED', paging).subscribe( response => {
      this.view.archivedThreadList = response;
      this.threadArchivedComponent.pagination.updatePaging(response.paging);
    }, e => console.error(e));
  }
}
