import { ForumService } from '../forum.service';
import { Observable } from 'rxjs/Observable';
import { CPagingParameter } from '../../app-common/app-common-definitions';

export class LoadPreviousPage implements DeleteItemResponseStrategy {
    supports(wasLastForumInPage: boolean, onlyOnePage: boolean): boolean {
        return wasLastForumInPage && !onlyOnePage;
    }
    handleResponse(): void {
    }
}

export class SpliceForumsArray implements DeleteItemResponseStrategy {
    supports(wasLastForumInPage: boolean, onlyOnePage: boolean): boolean {
        return onlyOnePage;
    }
    handleResponse(forums: ForumListEntryView[], forumId: number): ForumListEntryView[] {
        const forumIndex = forums.map(f => f.id).indexOf(forumId);
        if (forumIndex !== -1) {
            forums.splice(forumIndex, 1);
        }
        return forums;
    }
}

export class QueryForums implements DeleteItemResponseStrategy {
    supports(wasLastForumInPage: boolean, onlyOnePage: boolean): boolean {
        return !wasLastForumInPage && !onlyOnePage;
    }
    handleResponse(currentPage: number, forumService: ForumService): Observable<ForumListView> {
        const paging = new CPagingParameter();
        paging.pageNumber = currentPage;
        return forumService.query(paging);
    }
}

export class DeleteForumStrategyHandler {

    private strategies: DeleteItemResponseStrategy[] = [
        new LoadPreviousPage(),
        new SpliceForumsArray(),
        new QueryForums()
    ];

    constructor() {
    }

    getRightStrategy(wasLastForumInPage: boolean, onlyOnePage: boolean): DeleteItemResponseStrategy {
        return this.strategies.filter(s => s.supports(wasLastForumInPage, onlyOnePage))[0];
    }

}
