import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FORUM_LIST, } from './mock-forum-list';
import { ActivatedRoute } from '@angular/router';
import { ForumService } from '../forum.service';
import { DeleteDataModalComponent } from '../../app-common/delete-data-modal/delete-data-modal.component';
import { ApiService } from '../../app-common/api.service';
import { Observable } from 'rxjs/Observable';
import { QueryForums, SpliceForumsArray, LoadPreviousPage, DeleteForumStrategyHandler } from './delete-forum-strategy-handler';
import { PaginationComponent } from '../../app-common/pagination/pagination.component';
import { CSwitchForumPositionsRequest } from '../forum-definitions';
import { UserSessionService } from '../../app-common/user-session.service';
import { ItemsPerPage } from '../../app-common/app-common-definitions';

@Component({
  selector: 'app-forum-list',
  templateUrl: './forum-list.component.html',
  styleUrls: ['./forum-list.component.css']
})
export class ForumListComponent implements OnInit {

  @ViewChild(DeleteDataModalComponent) modal: DeleteDataModalComponent;
  @ViewChild(PaginationComponent) pagination: PaginationComponent;

  view: ForumListView;

  private deleteForumStrategyHandler: DeleteForumStrategyHandler;

  constructor(
    private route: ActivatedRoute,
    private forumService: ForumService,
    private apiService: ApiService,
    private userSessionService: UserSessionService
  ) { }

  ngOnInit() {
    this.view = this.route.snapshot.data['forumList'];
    this.deleteForumStrategyHandler = new DeleteForumStrategyHandler();
  }

  userIsAdmin(): boolean {
    return this.userSessionService.isAdmin;
  }

  openModal(forum: ForumListEntryView) {
    this.modal.showErrorMessage({ id: forum.id, name: forum.name });
  }

  deleteForum(forumId: number) {
    const lastItem = this.view.forums.length === 1;
    const onlyOnePage = this.view.paging.totalRecordCount <= ItemsPerPage;
    this.forumService.delete(forumId).subscribe(
      response => {
        this.modal.hide();
        this.handleDeleteResponse(lastItem, onlyOnePage, forumId);
      },
      e => console.error(e)
    );
  }

  onPagingByNumberEvent($event: PagingParameter) {
    this.forumService.query($event).subscribe(
      response => {
        this.view = response;
        this.pagination.handlePagingByNumberResponse($event.pageNumber);
      },
      e => console.error(e)
    );
  }

  handlePagingResponseEvents($event: ForumListView) {
    this.view = $event;
  }

  hasNoForums() {
    return !this.view.forums.length;
  }

  disableSortBtn(down: boolean, index: number) {
    const min = index === 0 && this.pagination.currentPage === 1;
    const max = index === this.view.forums.length - 1 &&
      this.pagination.currentPage === this.view.paging.maxPageCount;
    return !down && min || down && max;
  }

  sortUp(index: number) {
    this.sort(index, false);
  }

  sortDown(index: number) {
    this.sort(index, true);
  }

  private sort(index: number, sortDown: boolean) {
    // calc position of second forum to switch
    const posForumA = sortDown ? index + 1 : index - 1;
    // get references of both forums
    const forumA = this.view.forums[posForumA];
    const forumB = this.view.forums[index];
    // try to find indexes
    const forumAId = forumA ? forumA.id : null;
    const forumBId = forumB ? forumB.id : null;
    // create request
    const request = new CSwitchForumPositionsRequest(forumAId, forumBId, sortDown, this.pagination.currentPage);
    // make request
    this.forumService.switchForumPosition(request).subscribe(
      response => {
        if (response.forumNewInPage) {
          this.handleSortResponseForNewForumInPage(sortDown, response.forumNewInPage);
        } else {
          const copyForumA = { ...forumA };
          this.view.forums[posForumA] = forumB;
          this.view.forums[index] = copyForumA;
        }
      },
      e => console.error(e)
    );
  }

  private handleSortResponseForNewForumInPage(sortDown: boolean, forumNewInPage: ForumListEntryView) {
    if (sortDown) {
      this.view.forums.pop();
      this.view.forums.push(forumNewInPage);
    } else {
      this.view.forums[0] = forumNewInPage;
    }
  }

  private handleDeleteResponse(wasLastForumInPage: boolean, onlyOnePage: boolean, forumId: number) {
    const strategy = this.deleteForumStrategyHandler.getRightStrategy(wasLastForumInPage, onlyOnePage);

    switch (strategy.constructor) {
      case LoadPreviousPage:
        this.pagination.pagingPrev(true);
        break;
      case SpliceForumsArray:
        this.view.forums = (strategy as SpliceForumsArray)
          .handleResponse(this.view.forums, forumId);
        --this.view.paging.totalRecordCount;
        break;
      case QueryForums:
        (strategy as QueryForums)
          .handleResponse(this.pagination.currentPage, this.forumService)
          .subscribe(response => {
            this.view = response;
            this.pagination.updatePaging(response.paging);
          });
        break;
      default:
        break;
    }
  }

}
