export const FORUM_LIST: ForumListView = {
  paging: { maxPageCount: 10, totalRecordCount: 10, activePage: 1 , linkPrevPage: '', linkNextPage: '' },
  forums: [
    // tslint:disable-next-line:max-line-length
    { id: 1, name: 'NTAG Dingens Forum', type: 'Vorschlag', lastActivity: '15.12.2017', numberOfThreads: '13', creator: 'Sebastian Metermann' },
    // tslint:disable-next-line:max-line-length
    { id: 2, name: 'Projekt xY Forum', type: 'Ankündigung', lastActivity: '02.01.2018', numberOfThreads: '3', creator: 'Susi Sorglos' },
    // tslint:disable-next-line:max-line-length
    { id: 3, name: 'NTAG rulez', type: 'Diskussion', lastActivity: '03.01.2018', numberOfThreads: '22', creator: 'Hinz Kunz' },
    // tslint:disable-next-line:max-line-length
    { id: 4, name: 'Einfach so', type: 'Vorschlag', lastActivity: '15.12.2017', numberOfThreads: '13', creator: 'Susi Sorglos' },
    // tslint:disable-next-line:max-line-length
    { id: 5, name: 'Weil ich es kann', type: 'Ankündigung', lastActivity: '02.01.2018', numberOfThreads: '3', creator: 'Sebastian Metermann'  },
    // tslint:disable-next-line:max-line-length
    { id: 6, name: 'Morgen ist auch noch ein Tag', type: 'Diskussion', lastActivity: '03.01.2018', numberOfThreads: '22', creator: 'Hinz Kunz'  }
  ]
};
