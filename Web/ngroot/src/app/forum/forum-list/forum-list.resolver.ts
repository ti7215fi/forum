import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router/src/interfaces';
import { ForumService } from '../forum.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CPagingParameter } from '../../app-common/app-common-definitions';

@Injectable()
export class ForumListResolver implements Resolve<any> {
    constructor(
        private forumService: ForumService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.forumService.query(new CPagingParameter());
    }
}
