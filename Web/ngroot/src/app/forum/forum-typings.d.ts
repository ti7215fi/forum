declare interface ForumListView {
    forums: ForumListEntryView[];
    paging: Paging;
}

declare interface ForumListEntryView {
    id: number;
    name: string;
    type: string;
    lastActivity: string;
    numberOfThreads: string;
    creator: string;
}

declare interface ForumDetailView {
    name: string;
    type: string;
    description: string;
    threadList: ThreadListView;
    deletedThreadList: ThreadListView;
    archivedThreadList: ThreadListView;
    userCanCreateThread: boolean;
    paging: Paging;
}

declare interface ForumNewView {
    name: string;
    type: string;
    description: string;
    featureThreadStatus: boolean;
    roleReadForum: UserRole;
    roleCreateUpdateThread: UserRole;
    roleCreateUpdatePost: UserRole;
}

declare interface ForumEditView {
    name: string;
    type: string;
    description: string;
    featureThreadStatus: boolean;
    roleReadForum: string;
    roleCreateUpdateThread: string;
    roleCreateUpdatePost: string;
}

declare interface SwitchForumPositionsRequest {
    forumAId: number;
    forumBId: number;
    sortDown: boolean;
    currentPage: number;
}

declare interface SwitchForumPositionsResponse {
    forumNewInPage: ForumListEntryView;
}

declare interface SwitchForumView {
    id: number;
    position: number;
}