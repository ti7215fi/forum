export declare type ForumType = 'ANNOUNCEMENT' | 'PROPOSAL' | 'DISCUSSION';
export declare interface ForumTypeSelectOption { key: ForumType; value: string; }

export const FORUM_TYPES: ForumTypeSelectOption[] = [
    { key: 'ANNOUNCEMENT', value: 'Ankündigung' },
    { key: 'PROPOSAL', value: 'Vorschlag' },
    { key: 'DISCUSSION', value: 'Diskussion' },
];
