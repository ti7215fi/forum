import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FORUM_TYPES, ForumTypeSelectOption } from '../forum-types';
import { ForumService } from '../forum.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { QuillEditorComponent } from 'ngx-quill';
import { QUILL_EDITOR_MODULES } from '../../app-common/quill-editor-options';

@Component({
  selector: 'app-forum-edit',
  templateUrl: './forum-edit.component.html',
  styleUrls: ['./forum-edit.component.css']
})
export class ForumEditComponent implements OnInit {

  @ViewChild(QuillEditorComponent) editor: QuillEditorComponent;
  editorModules = QUILL_EDITOR_MODULES;

  originalData: ForumEditView;
  forum: ForumEditView;
  forumId: number;
  forumTypes: ForumTypeSelectOption[];

  // Validation
  rForm: FormGroup;
  name: FormControl;
  type: FormControl;
  description: FormControl;
  titleAlert = 'Dies ist ein Pflichfeld!';
  updateSuccess: boolean;
  updateFailed: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private forumService: ForumService
  ) { }

  ngOnInit() {
    this.initData();
    this.createFormControls();
    this.createForm();
  }

  update() {
    this.forumService.update(this.forumId, this.forum).subscribe(
      response => {
        this.updateFailed = false;
        this.updateSuccess = true;
      },
      e => {
        this.updateFailed = true;
        this.updateSuccess = false;
        console.error(e);
      }
    );
  }

  private initData() {
    this.forumTypes = FORUM_TYPES;
    this.originalData = this.route.snapshot.data['forum'];
    this.forum = { ...this.originalData };
    this.route.parent.params.subscribe(params => {
      this.forumId = params['forumId'];
    });
    this.updateSuccess = false;
    this.updateFailed = false;
  }

  private createFormControls() {
    this.name = new FormControl('', Validators.required);
    this.type = new FormControl('', Validators.required);
    this.description = new FormControl('', Validators.required);
  }

  private createForm() {
    this.rForm = new FormGroup({
      name: this.name,
      type: this.type,
      description: this.description
    });
  }

}
