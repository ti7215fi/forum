import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ForumService } from '../forum.service';

@Injectable()
export class ForumEditResolver implements Resolve<any> {
    constructor(
        private forumService: ForumService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.forumService.get(route.parent.params['forumId']);
    }
}
