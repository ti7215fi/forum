import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { TokenService } from '../token.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { UserSessionService } from '../../app-common/user-session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  // Validation
  rForm: FormGroup;
  fcUsername: FormControl;
  fcPassword: FormControl;
  titleAlert = 'Dies ist ein Pflichfeld!';

  constructor(
    private router: Router,
    private authService: AuthService,
    private tokenService: TokenService,
    private userSessionService: UserSessionService
  ) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  login() {
    this.authService.login(this.username, this.password).subscribe(
      (response: LoginResponse) => {
        this.userSessionService.isAdmin = response.isAdmin;
        this.tokenService.storeToken(response.token);
        this.router.navigate(['/forums']);
      },
      e => console.error(e)
    );
  }

  private createFormControls() {
    this.fcUsername = new FormControl('', Validators.required);
    this.fcPassword = new FormControl('', Validators.required);
  }

  private createForm() {
    this.rForm = new FormGroup({
      fcUsername: this.fcUsername,
      fcPassword: this.fcPassword
    });
  }

}
