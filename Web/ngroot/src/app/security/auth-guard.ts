import { CanActivate } from '@angular/router/src/interfaces';
import { TokenService } from './token.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private tokenService: TokenService
    ) {

    }

    canActivate(route, state): boolean {
        return this.tokenService.retrieveToken() !== null;
    }
}
