import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(userName: string, password: string) {
    const request: LoginRequest = {
      username: btoa(userName),
      password: btoa(password)
    };
    return this.http.post('http://localhost:28903/api/auth/login', request);
  }

}
