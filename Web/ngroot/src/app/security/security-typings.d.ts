declare interface LoginRequest {
    username: string;
    password: string;
}

declare interface LoginResponse {
    token: string;
    isAdmin: boolean;
}

declare type UserRole = 'ADMIN' | 'DEFAULT_USER';