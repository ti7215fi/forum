import { Injectable } from '@angular/core';
import { tokenKey } from '@angular/core/src/view/util';

@Injectable()
export class TokenService {

  // tslint:disable-next-line:no-inferrable-types
  private tokenKey: string = 'app_token';

  constructor() { }

  storeToken(token: string) {
    sessionStorage.setItem(this.tokenKey, token);
  }

  retrieveToken(): string {
    return sessionStorage.getItem(this.tokenKey);
  }

  removeToken() {
    sessionStorage.removeItem(this.tokenKey);
  }

}
