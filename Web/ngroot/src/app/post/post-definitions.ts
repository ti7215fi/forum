export class CPostNewView implements PostNewView {
    contentHtml: string;
    contentPlain: string;
    threadId: number;
}

export class CPostEditView implements PostEditView {
    contentHtml: string;
    contentPlain: string;
}
