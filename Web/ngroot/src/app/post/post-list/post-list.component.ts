import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../app-common/api.service';
import { ThreadService } from '../../thread/thread.service';
import { DeletePostStrategyHandler, LoadPreviousPage, QueryPosts, SplicePostsArray } from './delete-post-strategy-handler';
import { PaginationComponent } from '../../app-common/pagination/pagination.component';
import { ItemsPerPage } from '../../app-common/app-common-definitions';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  @Input() view: PostListView;
  @Input() threadId: number;
  @Input() postToHighlight;

  @ViewChild(PaginationComponent) pagination: PaginationComponent;

  private deletePostStrategyHandler: DeletePostStrategyHandler;

  constructor(
    private apiService: ApiService,
    private threadService: ThreadService
  ) { }

  ngOnInit() {
    this.deletePostStrategyHandler = new DeletePostStrategyHandler();
  }

  onPostDeleted(post: PostListEntryView) {
    const lastPost = this.view.posts.length === 1;
    const onlyOnePage = this.view.paging.totalRecordCount <= ItemsPerPage;
    const strategy = this.deletePostStrategyHandler.getRightStrategy(lastPost, onlyOnePage);

    switch (strategy.constructor) {
      case LoadPreviousPage:
        this.pagination.pagingPrev(true);
        break;
      case QueryPosts:
        (strategy as QueryPosts)
          .handleResponse(this.pagination.currentPage, this.threadId, this.threadService)
          .subscribe(response => {
            this.view = response;
            this.pagination.updatePaging(response.paging);
          });
        break;
      case SplicePostsArray:
        this.view.posts = (strategy as SplicePostsArray)
          .handleResponse(this.view.posts, post.id);
        --this.view.paging.totalRecordCount;
        break;
      default:
        break;
    }
  }

  onPostCreated(newMaxPageCount: number) {
    if (this.pagination.pagingNumbers.length < newMaxPageCount) {
      this.pagination.pagingNumbers.push(newMaxPageCount);
    }
    ++this.view.paging.totalRecordCount;
  }

  onPagingByNumberEvent($event: PagingParameter) {
    this.threadService.getThreadPosts(this.threadId, $event).subscribe(
      response => {
        this.view = response;
        this.pagination.handlePagingByNumberResponse($event.pageNumber);
      },
      e => console.error(e)
    );
  }

  handlePagingResponseEvents($event: PostListView) {
    this.view = $event;
  }

  getStyle(post: PostListEntryView) {
    /* tslint:disable:triple-equals */
    if (post.id == this.postToHighlight) {
      return {
        'border': 'solid #1f9b7c',
      };
    }
    /* tslint:enable:triple-equals */
  }

}
