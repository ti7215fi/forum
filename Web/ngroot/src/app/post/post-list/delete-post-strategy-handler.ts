import { Observable } from 'rxjs/Observable';
import { ThreadService } from '../../thread/thread.service';
import { CPagingParameter } from '../../app-common/app-common-definitions';

export class LoadPreviousPage implements DeleteItemResponseStrategy {
  supports(wasLastPostInPage: boolean, onlyOnePage: boolean): boolean {
    return wasLastPostInPage && !onlyOnePage;
  }
  handleResponse(): void {
  }
}

export class SplicePostsArray implements DeleteItemResponseStrategy {
  supports(wasLastPostInPage: boolean, onlyOnePage: boolean): boolean {
    return onlyOnePage;
  }
  handleResponse(posts: PostListEntryView[], postId: number): PostListEntryView[] {
    const forumIndex = posts.map(f => f.id).indexOf(postId);
    if (forumIndex !== -1) {
      posts.splice(forumIndex, 1);
    }
    return posts;
  }
}

export class QueryPosts implements DeleteItemResponseStrategy {
  supports(wasLastPostInPage: boolean, onlyOnePage: boolean): boolean {
    return !wasLastPostInPage && !onlyOnePage;
  }
  handleResponse(currentPage: number, threadId: number, threadService: ThreadService): Observable<PostListView> {
    const paging = new CPagingParameter();
    paging.pageNumber = currentPage;
    return threadService.getThreadPosts(threadId, paging);
  }
}

export class DeletePostStrategyHandler {

  private strategies: DeleteItemResponseStrategy[] = [
    new LoadPreviousPage(),
    new SplicePostsArray(),
    new QueryPosts()
  ];

  constructor() {
  }

  getRightStrategy(wasLastPostInPage: boolean, onlyOnePage: boolean): DeleteItemResponseStrategy {
    return this.strategies.filter(s => s.supports(wasLastPostInPage, onlyOnePage))[0];
  }

}
