import { Observable } from 'rxjs/Observable';
import { ThreadService } from '../../thread/thread.service';
import { CPagingParameter } from '../../app-common/app-common-definitions';

export interface CreatePostResponseStrategy {
    supports(maxItemsInPage: boolean, isFirstPage: boolean): boolean;
    handleResponse(...args): any;
}

export class UnshiftPostStrategy implements CreatePostResponseStrategy {
    supports(maxItemsInPage: boolean, isFirstPage: boolean): boolean {
        return !maxItemsInPage && isFirstPage;
    }
    handleResponse(threads: PostListEntryView[], thread: PostListEntryView): PostListEntryView[] {
        threads.unshift(thread);
        return threads;
    }
}

export class UnshiftAndPopPostStrategy implements CreatePostResponseStrategy {
    supports(maxItemsInPage: boolean, isFirstPage: boolean): boolean {
        return maxItemsInPage && isFirstPage;
    }
    handleResponse(threads: PostListEntryView[], thread: PostListEntryView): PostListEntryView[] {
        threads.pop();
        threads.unshift(thread);
        return threads;
    }
}

export class QueryPosts implements CreatePostResponseStrategy {
    supports(maxItemsInPage: boolean, isFirstPage: boolean): boolean {
        return !isFirstPage;
    }
    handleResponse(currentPage: number, threadId: number, threadService: ThreadService): Observable<PostListView> {
        const paging = new CPagingParameter();
        paging.pageNumber = currentPage;
        return threadService.getThreadPosts(threadId, paging);
    }
}

export class CreatePostResponseStrategyHandler {

    private readonly strategies: CreatePostResponseStrategy[] = [
        new UnshiftPostStrategy(),
        new UnshiftAndPopPostStrategy(),
        new QueryPosts(),
    ];

    constructor() {
    }

    getRightStrategy(maxItemsInPage: boolean, onlyOnePage: boolean): CreatePostResponseStrategy {
        return this.strategies.filter(s => s.supports(maxItemsInPage, onlyOnePage))[0];
    }

}
