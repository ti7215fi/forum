import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../post.service';
import { Response } from '@angular/http';
import { CPostNewView } from '../post-definitions';
import { QuillEditorComponent } from 'ngx-quill';
import { QUILL_EDITOR_MODULES } from '../../app-common/quill-editor-options';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {

  @ViewChild(QuillEditorComponent) editor: QuillEditorComponent;
  editorModules = QUILL_EDITOR_MODULES;

  @Output() createPostEvent = new EventEmitter<PostCreateResponse>();

  post: PostNewView;

  // Validation
  rForm: FormGroup;
  postDescription: FormControl;
  titleAlert = 'Geben Sie einen Beitrag ein!';

  private showErrorMessage: boolean;

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
  ) { }

  ngOnInit() {
    this.initData();
    this.createFormControls();
    this.createForm();
  }

  create() {
    this.post.contentPlain = this.editor.editorElem.textContent;
    this.postService.create(this.post).subscribe(
      response => {
        this.createPostEvent.emit(response);
        this.rForm.reset();
      },
      e => console.error(e)
    );
  }

  private initData() {
    this.post = new CPostNewView();
    this.route.parent.params.subscribe(params => {
      this.post.threadId = params['threadId'];
    });
    this.showErrorMessage = false;
  }

  private createFormControls() {
    this.postDescription = new FormControl('', Validators.required);
  }

  private createForm() {
    this.rForm = new FormGroup({
      postDescription: this.postDescription
    });
  }

}
