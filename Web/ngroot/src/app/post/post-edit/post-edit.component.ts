import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { PostService } from '../post.service';
import { DeleteDataModalComponent } from '../../app-common/delete-data-modal/delete-data-modal.component';
import { CPostEditView } from '../post-definitions';
import { QuillEditorComponent } from 'ngx-quill';
import { QUILL_EDITOR_MODULES } from '../../app-common/quill-editor-options';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit {

  @ViewChild(QuillEditorComponent) editor: QuillEditorComponent;
  @ViewChild(DeleteDataModalComponent) modal: DeleteDataModalComponent;

  @Input() post: PostListEntryView;

  @Output() postDeleted: EventEmitter<PostListEntryView> = new EventEmitter();

  editorModules = QUILL_EDITOR_MODULES;

  updatedPost: PostEditView;
  editMode: boolean;
  editorOptions: any;

  rForm: FormGroup;
  editPostDescription: FormControl;
  titleAlert = 'Geben Sie einen Beitrag ein!';

  constructor(
    private postService: PostService
  ) { }

  ngOnInit() {
    this.initData();
    this.createFormControls();
    this.createForm();
  }

  update() {
    this.updatedPost.contentPlain = this.editor.editorElem.textContent;
    this.postService.update(this.post.id, this.updatedPost).subscribe(
      response => this.handleResponse(response),
      e => console.error(e)
    );
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
  }

  openModal(post: PostListEntryView) {
    this.modal.showErrorMessage({ id: post.id, name: '' });
  }

  deletePost(postId) {
    this.postService.delete(postId).subscribe(
      response => {
        this.modal.hide();
        this.postDeleted.emit(this.post);
      },
      e => console.error(e)
    );
  }

  private handleResponse(response: PostListEntryView) {
    this.post = response;
    this.updatedPost.contentHtml = response.content;
    this.toggleEditMode();
  }

  private initData() {
    this.editMode = false;
    this.updatedPost = new CPostEditView();
    this.updatedPost.contentHtml = this.post.content;
  }

  private createFormControls() {
    this.editPostDescription = new FormControl('', Validators.required);
  }

  private createForm() {
    this.rForm = new FormGroup({
      editPostDescription: this.editPostDescription
    });
  }

}
