declare interface PostListEntryView {
    id: number;
    creator: string;
    createdAt: string;
    updatedAt: string;
    content: string;
    canEdit: boolean;
}

declare interface PostListView {
    posts: PostListEntryView[];
    paging: Paging;
    activePage: number;
}

declare interface PostNewView {
    threadId: number;
    contentHtml: string;
    contentPlain: string;
}

declare interface PostEditView {
    contentHtml: string;
    contentPlain: string;
}

declare interface PostCreateResponse {
    post: PostListEntryView;
    maxPageCount: number;
}