import { Injectable } from '@angular/core';
import { ApiService } from '../app-common/api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PostService {

  private readonly API_ROOT_URL: string = 'posts';

  constructor(
    private apiService: ApiService
  ) { }

  create(post: PostNewView): Observable<PostCreateResponse> {
    return this.apiService.authPost(this.API_ROOT_URL, post);
  }

  update(id, post: PostEditView) {
    return this.apiService.authPut(this.API_ROOT_URL + '/' + id, post);
  }

  delete(id) {
    return this.apiService.authDelete(this.API_ROOT_URL + '/' + id);
  }

}
