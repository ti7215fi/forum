import { Component, OnInit, ViewChild } from '@angular/core';
import { ThreadService } from '../thread.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { UserSessionService } from '../../app-common/user-session.service';
import { CThreadNewView } from '../thread-definitions';
import { QuillEditorComponent } from 'ngx-quill';
import { QUILL_EDITOR_MODULES } from '../../app-common/quill-editor-options';

@Component({
  selector: 'app-thread-create',
  templateUrl: './thread-create.component.html',
  styleUrls: ['./thread-create.component.css']
})
export class ThreadCreateComponent implements OnInit {

  @ViewChild(QuillEditorComponent) editor: QuillEditorComponent;
  editorModules = QUILL_EDITOR_MODULES;

  forumId: number;
  forumName: string;
  thread: ThreadNewView;

  // Validation
  rForm: FormGroup;
  title: FormControl;
  threadDescription: FormControl;
  titleAlert = 'Dies ist ein Pflichfeld!';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private threadService: ThreadService,
    private userSessionService: UserSessionService
  ) { }

  ngOnInit() {
    this.initData();
    this.createFormControls();
    this.createForm();
  }

  get userIsAdmin(): boolean {
    return this.userSessionService.isAdmin;
  }

  create() {
    this.thread.descriptionPlain = this.editor.editorElem.textContent;
    this.threadService.create(this.thread).subscribe(
      response => this.router.navigate(['forum', this.forumId]),
      e => console.error(e)
    );
  }

  getMinWidthForForm(): string {
    return !this.userIsAdmin ? '100%' : '';
  }

  private initData() {
    this.thread = new CThreadNewView();
    this.route.parent.params.subscribe(params => {
      this.forumId = params['forumId'];
      this.thread.forumId = this.forumId;
    });
    this.forumName = this.userSessionService.forumName;
  }

  private createFormControls() {
    this.title = new FormControl('', Validators.required);
    this.threadDescription = new FormControl('', Validators.required);
  }

  private createForm() {
    this.rForm = new FormGroup({
      title: this.title,
      threadDescription: this.threadDescription
    });
  }

}
