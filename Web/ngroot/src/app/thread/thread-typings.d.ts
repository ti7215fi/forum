declare interface ThreadListView {
    forumHasStatusFeature: boolean;
    threads: ThreadListEntryView[];
    paging: Paging;
}

declare interface ThreadListEntryView {
    id: number;
    title: string;
    description: string;
    creator: string;
    lastActivity: string;
    status: string;
    unlocked: boolean;
    isDeleted: boolean;
    hasCreated: boolean;
}

declare interface ThreadDetailView {
    forumId: number;
    forumName: string;
    forumType: string;
    createdAt: string;
    updatedAt: string;
    title: string;
    description: string;
    status: string;
    creator: string;
    userCanCreatePost: boolean;
    forumHasStatusFeature: boolean;
    postList: PostListView;
    paging: Paging;
}

declare interface ThreadNewView {
    forumId: number;
    title: string;
    descriptionHtml: string;
    descriptionPlain: string;
    unlock: boolean;
}

declare interface ThreadEditRequest {
    title: string;
    descriptionHtml: string;
    descriptionPlain: string;
    unlock: boolean;
}

declare interface ThreadEditResponse {
    forumId: number;
    forumName: string;
    title: string;
    description: string;
    unlock: boolean;
}

declare type ThreadType = 'ACTIVE' | 'DELETED' | 'ARCHIVED';
declare type ThreadStatus = 'REJECTED' | 'REVIEW' | 'ACCEPTED';
declare type ThreadStatusSelectOption = { key: ThreadStatus, value: string };
