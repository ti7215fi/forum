export class CThreadNewView implements ThreadNewView {
    descriptionHtml: string;
    descriptionPlain: string;
    forumId: number;
    title: string;
    unlock: boolean;

    constructor() {
        this.unlock = false;
    }
}

export const THREAD_STATUS_REJECTED: ThreadStatus = 'REJECTED';
export const THREAD_STATUS_REVIEW: ThreadStatus = 'REVIEW';
export const THREAD_STATUS_ACCEPTED: ThreadStatus = 'ACCEPTED';

export const THREAD_STATUS: ThreadStatusSelectOption[] = [
    { key: THREAD_STATUS_REJECTED, value: 'wird nicht umgesetzt' },
    { key: THREAD_STATUS_REVIEW, value: 'in Prüfung' },
    { key: THREAD_STATUS_ACCEPTED, value: 'wird umgesetzt' },
];
