import { Observable } from 'rxjs/Observable';
import { ThreadService } from './thread.service';
import { ForumService } from './../forum/forum.service';
import { CPagingParameter } from '../app-common/app-common-definitions';

export class LoadPreviousPage implements DeleteItemResponseStrategy {
  supports(wasLastThreadInPage: boolean, onlyOnePage: boolean): boolean {
    return wasLastThreadInPage && !onlyOnePage;
  }
  handleResponse(): void {
  }
}

export class SpliceThreadsArray implements DeleteItemResponseStrategy {
  supports(wasLastThreadInPage: boolean, onlyOnePage: boolean): boolean {
    return onlyOnePage;
  }
  handleResponse(threads: ThreadListEntryView[], threadId: number): ThreadListEntryView[] {
    const forumIndex = threads.map(f => f.id).indexOf(threadId);
    if (forumIndex !== -1) {
      threads.splice(forumIndex, 1);
    }
    return threads;
  }
}

export class QueryThreads implements DeleteItemResponseStrategy {
  supports(wasLastThreadInPage: boolean, onlyOnePage: boolean): boolean {
    return !wasLastThreadInPage && !onlyOnePage;
  }
  handleResponse(currentPage: number, forumId: number, whichTreads: ThreadType, forumService: ForumService): Observable<ThreadListView> {
    const paging = new CPagingParameter();
    paging.pageNumber = currentPage;
    return forumService.getForumThreads(forumId, whichTreads, paging);
  }
}

export class DeleteThreadStrategyHandler {

  private strategies: DeleteItemResponseStrategy[] = [
    new LoadPreviousPage(),
    new SpliceThreadsArray(),
    new QueryThreads()
  ];

  constructor() {
  }

  getRightStrategy(wasLastThreadInPage: boolean, onlyOnePage: boolean): DeleteItemResponseStrategy {
    return this.strategies.filter(s => s.supports(wasLastThreadInPage, onlyOnePage))[0];
  }

}
