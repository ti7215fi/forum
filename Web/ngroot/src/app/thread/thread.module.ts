import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThreadListComponent } from './thread-list/thread-list.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from '../app-routing/app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { ThreadDetailComponent } from './thread-detail/thread-detail.component';
import { ThreadService } from './thread.service';
import { PostModule } from '../post/post.module';
import { ThreadDetailResolver } from './thread-detail/thread-detail.resolver';
import { ThreadCreateComponent } from './thread-create/thread-create.component';
import { ThreadEditComponent } from './thread-edit/thread-edit.component';
import { ThreadComponent } from './thread/thread.component';
import { ThreadEditResolver } from './thread-edit/thread-edit.resolver';
import { TranslateThreadStatusPipe } from './translate-thread-status.pipe';
import { AppCommonModule } from '../app-common/app-common.module';
import { SearchModule } from '../search/search.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThreadDeletedComponent } from './thread-deleted/thread-deleted.component';
import { QuillModule } from 'ngx-quill';
import { ThreadArchivedComponent } from './thread-archived/thread-archived.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserModule,
    PostModule,
    QuillModule,
    AppCommonModule,
    SearchModule
  ],

  exports: [
    ThreadListComponent,
    ThreadDeletedComponent,
    ThreadArchivedComponent
  ],

  declarations: [
    ThreadListComponent,
    ThreadDetailComponent,
    ThreadCreateComponent,
    ThreadEditComponent,
    ThreadComponent,
    TranslateThreadStatusPipe,
    ThreadDeletedComponent,
    ThreadArchivedComponent,
  ],

  providers: [
    ThreadService,
    ThreadDetailResolver,
    ThreadEditResolver
  ]
})
export class ThreadModule { }
