import { Pipe, PipeTransform } from '@angular/core';
import { THREAD_STATUS } from './thread-definitions';

@Pipe({
  name: 'translateThreadStatus'
})
export class TranslateThreadStatusPipe implements PipeTransform {

  transform(key: ThreadStatus, args?: any): any {
    if (!key) {
      return 'Status ausstehend';
    }
    const status = THREAD_STATUS.filter( s => s.key === key )[0];
    return status ? status.value : 'Nicht definierter Status';
  }

}
