import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { PaginationComponent } from '../../app-common/pagination/pagination.component';
import { ThreadService } from '../thread.service';
import { ForumService } from '../../forum/forum.service';
import { ApiService } from '../../app-common/api.service';
import { DeleteDataModalComponent } from '../../app-common/delete-data-modal/delete-data-modal.component';
import { DeleteThreadStrategyHandler, LoadPreviousPage, SpliceThreadsArray, QueryThreads } from '../delete-thread-strategy-handler';
import { ItemsPerPage } from '../../app-common/app-common-definitions';

@Component({
  selector: 'app-thread-archived',
  templateUrl: './thread-archived.component.html',
  styleUrls: ['./thread-archived.component.css']
})
export class ThreadArchivedComponent implements OnInit {

  @Input() view: ThreadListView;
  @Input() forumId: number;

  @Output() recoverThreadEvent = new EventEmitter<ThreadListEntryView>();

  @ViewChild(PaginationComponent) pagination: PaginationComponent;
  @ViewChild(DeleteDataModalComponent) modal: DeleteDataModalComponent;

  deleteThreadStrategyHandler: DeleteThreadStrategyHandler;

  constructor(
    private threadService: ThreadService,
    private forumService: ForumService,
    private apiService: ApiService
  ) { }

  ngOnInit() {
    this.deleteThreadStrategyHandler = new DeleteThreadStrategyHandler();
    this.view.threads = !this.view.threads ? [] : this.view.threads;
  }

  openModal(thread: ThreadListEntryView) {
    this.modal.showErrorMessage({ id: thread.id, name: thread.title });
  }

  recoverThread(threadId) {
    const lastThread = this.view.threads.length === 1;
    const onlyOnePage = this.view.paging.totalRecordCount <= ItemsPerPage;
    this.threadService.patchRecoverThread(threadId).subscribe(
      response => {
        const threadIndex = this.view.threads.map(t => t.id).indexOf(threadId);
        const copyOfRecoveredThread = { ...this.view.threads[threadIndex] };
        this.handleDeleteResponse(lastThread, onlyOnePage, threadId);
        this.recoverThreadEvent.emit(copyOfRecoveredThread);
      },
      e => console.error(e)
    );
  }

  deleteThread(threadId) {
    const lastThread = this.view.threads.length === 1;
    const onlyOnePage = this.view.paging.totalRecordCount <= ItemsPerPage;
    this.threadService.delete(threadId, true).subscribe(
      response => {
        const threadIndex = this.view.threads.map(t => t.id).indexOf(threadId);
        this.handleDeleteResponse(lastThread, onlyOnePage, threadId);
        this.modal.hide();
      },
      e => console.error(e)
    );
  }

  onPagingByNumberEvent($event: PagingParameter) {
    this.forumService.getForumThreads(this.forumId, 'ARCHIVED', $event).subscribe(
      response => {
        this.view = response;
        this.pagination.handlePagingByNumberResponse($event.pageNumber);
      },
      e => console.error(e)
    );
  }

  handlePagingResponseEvents($event: ThreadListView) {
    this.view = $event;
  }

  hasNoArchivedThreads() {
    return !this.view.threads.length;
  }

  private handleDeleteResponse(wasLastThreadInPage: boolean, onlyOnePage: boolean, threadId: number) {
    const strategy = this.deleteThreadStrategyHandler.getRightStrategy(wasLastThreadInPage, onlyOnePage);

    switch (strategy.constructor) {
      case LoadPreviousPage:
        this.pagination.pagingPrev(true);
        break;
      case SpliceThreadsArray:
        this.view.threads = (strategy as SpliceThreadsArray)
          .handleResponse(this.view.threads, threadId);
        --this.view.paging.totalRecordCount;
        break;
      case QueryThreads:
        (strategy as QueryThreads)
          .handleResponse(this.pagination.currentPage, this.forumId, 'ARCHIVED', this.forumService)
          .subscribe(response => {
            this.view = response;
            this.pagination.updatePaging(response.paging);
          });
        break;
      default:
        break;
    }
  }

}
