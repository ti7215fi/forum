import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadArchivedComponent } from './thread-archived.component';

describe('ThreadArchivedComponent', () => {
  let component: ThreadArchivedComponent;
  let fixture: ComponentFixture<ThreadArchivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreadArchivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadArchivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
