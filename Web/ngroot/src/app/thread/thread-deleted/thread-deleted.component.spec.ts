import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadDeletedComponent } from './thread-deleted.component';

describe('ThreadDeletedComponent', () => {
  let component: ThreadDeletedComponent;
  let fixture: ComponentFixture<ThreadDeletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreadDeletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadDeletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
