import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router/src/interfaces';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ThreadService } from '../thread.service';
import { CPagingParameter } from '../../app-common/app-common-definitions';

@Injectable()
export class ThreadDetailResolver implements Resolve<any> {
    constructor(
        private threadService: ThreadService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const threadId = route.parent.params['threadId'];
        const postToHighlightId = route.queryParams['highlightPostId'];
        return this.threadService.getThreadDetails(threadId, new CPagingParameter(), postToHighlightId);
    }
}
