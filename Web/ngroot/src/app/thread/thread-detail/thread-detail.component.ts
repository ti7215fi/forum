import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ThreadService } from '../thread.service';
import { PostListComponent } from '../../post/post-list/post-list.component';
import { THREAD_STATUS, THREAD_STATUS_ACCEPTED, THREAD_STATUS_REJECTED, THREAD_STATUS_REVIEW } from '../thread-definitions';
import {
  CreatePostResponseStrategyHandler, UnshiftPostStrategy,
  QueryPosts, UnshiftAndPopPostStrategy
} from '../../post/post-create/post-create-response-strategy-handler';
import { UserSessionService } from '../../app-common/user-session.service';
import { ItemsPerPage } from '../../app-common/app-common-definitions';

@Component({
  selector: 'app-thread-detail',
  templateUrl: './thread-detail.component.html',
  styleUrls: ['./thread-detail.component.css']
})
export class ThreadDetailComponent implements OnInit {

  @ViewChild(PostListComponent) postListComponent: PostListComponent;

  statuses = THREAD_STATUS;

  view: ThreadDetailView;
  newPost: PostNewView;
  threadId: number;
  postToHighlight;

  statusUpdateSuccess: boolean;
  statusUpdateFailed: boolean;

  private createPostResponseStrategyHandler: CreatePostResponseStrategyHandler;

  constructor(
    private route: ActivatedRoute,
    private userSessionService: UserSessionService,
    private threadService: ThreadService
  ) { }

  ngOnInit() {
    this.view = this.route.snapshot.data['threadDetails'];
    this.postToHighlight = this.route.snapshot.queryParams['highlightPostId'];
    const stub = this.route.parent.params.subscribe(params => {
      this.threadId = +params['threadId'];
    });
    this.createPostResponseStrategyHandler = new CreatePostResponseStrategyHandler();
    this.statusUpdateSuccess = false;
    this.statusUpdateFailed = false;
  }

  userIsAdmin(): boolean {
    return this.userSessionService.isAdmin;
  }

  addNewPost($event: PostCreateResponse) {
    const isFirstPage = this.postListComponent.pagination.currentPage === 1;
    const maxItemsInPage = this.view.postList.posts.length === ItemsPerPage;
    const strategy = this.createPostResponseStrategyHandler.getRightStrategy(maxItemsInPage, isFirstPage);

    switch (strategy.constructor) {
      case UnshiftPostStrategy:
        this.postListComponent.view.posts = (strategy as UnshiftPostStrategy)
          .handleResponse(this.view.postList.posts, $event.post);
        break;
      case UnshiftAndPopPostStrategy:
        this.postListComponent.view.posts = (strategy as UnshiftAndPopPostStrategy)
          .handleResponse(this.view.postList.posts, $event.post);
        this.postListComponent.onPostCreated($event.maxPageCount);
        break;
      case QueryPosts:
        (strategy as QueryPosts)
          .handleResponse(this.postListComponent.pagination.currentPage, this.threadId, this.threadService).subscribe(response => {
            this.view.postList = response;
          }, e => console.error(e));
        this.postListComponent.onPostCreated($event.maxPageCount);
        break;
      default:
        console.error('Unkown strategy!');
        break;
    }
  }

  onStatusChange(status: ThreadStatus) {
    this.threadService.patchUpdateStatus(this.threadId, status).subscribe(
      response => {
        this.statusUpdateSuccess = true;
        this.statusUpdateFailed = false;
      },
      error => {
        this.statusUpdateFailed = true;
        this.statusUpdateSuccess = false;
        console.error(error);
      }
    );
  }

  closeStatusMessage() {
    this.statusUpdateFailed = false;
    this.statusUpdateSuccess = false;
  }

  getClassForStatus(): string {
    switch (this.view.status) {
      case THREAD_STATUS_ACCEPTED:
        return 'alert-success';
      case THREAD_STATUS_REJECTED:
        return 'alert-danger';
      case THREAD_STATUS_REVIEW:
        return 'alert-warning';
      default:
        return 'alert-info';
    }
  }

}
