import { Injectable } from '@angular/core';
import { ApiService } from '../app-common/api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ThreadService {

  private readonly API_ROOT_URL: string = 'threads';

  constructor(
    private apiService: ApiService
  ) { }

  get(id) {
    return this.apiService.authGet(this.API_ROOT_URL + '/' +  id);
  }

  getThreadDetails(id: number, pagingParameter: PagingParameter, postToHighlight: number = null): Observable<ThreadDetailView> {
    return this.apiService.authGet(`${this.API_ROOT_URL}/${id}/details` +
    `?postToHighlight=${postToHighlight}` +
    `&pageNumber=${pagingParameter.pageNumber}` +
    `&pageSize=${pagingParameter.pageSize}`);
  }

  getThreadPosts(id: number, pagingParameter: PagingParameter): Observable<PostListView> {
    return this.apiService.authGet(`${this.API_ROOT_URL}/${id}/posts` +
    `?pageNumber=${pagingParameter.pageNumber}` +
    `&pageSize=${pagingParameter.pageSize}`);
  }

  create(thread: ThreadNewView) {
    return this.apiService.authPost(this.API_ROOT_URL, thread);
  }

  update(id, thread: ThreadEditRequest) {
    return this.apiService.authPut(this.API_ROOT_URL + '/' + id, thread);
  }

  delete(id, hardDelete: boolean) {
    return this.apiService.authDelete(this.API_ROOT_URL + '/' + id + '?hardDelete=' + hardDelete);
  }

  patchUnlockThread(id, unlock: boolean) {
    return this.apiService.authPatch(this.API_ROOT_URL + '/' + id + '/unlock?unlock=' + unlock);
  }

  patchRecoverThread(id) {
    return this.apiService.authPatch(this.API_ROOT_URL + '/' + id + '/recover');
  }

  patchArchiveThread(id) {
    return this.apiService.authPatch(this.API_ROOT_URL + '/' + id + '/archive');
  }

  patchUpdateStatus(id, status: string) {
    return this.apiService.authPatch(this.API_ROOT_URL + '/' + id + '/update-status?status=' + status);
  }

}
