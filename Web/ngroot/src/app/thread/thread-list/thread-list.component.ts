import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { FORUM_DETAIL_LIST } from './mock-forum-detail';
import { ThreadService } from '../thread.service';
import { DeleteDataModalComponent } from '../../app-common/delete-data-modal/delete-data-modal.component';
import { UserSessionService } from '../../app-common/user-session.service';
import { ForumService } from '../../forum/forum.service';
import { ApiService } from '../../app-common/api.service';
import { DeleteThreadStrategyHandler, LoadPreviousPage, SpliceThreadsArray, QueryThreads } from '../delete-thread-strategy-handler';
import { PaginationComponent } from '../../app-common/pagination/pagination.component';
import { ItemsPerPage } from '../../app-common/app-common-definitions';

@Component({
  selector: 'app-thread-list',
  templateUrl: './thread-list.component.html',
  styleUrls: ['./thread-list.component.css']
})
export class ThreadListComponent implements OnInit {

  @Input() view: ThreadListView;
  @Input() forumId: number;

  @Output() archiveThreadEvent = new EventEmitter<void>();

  @ViewChild(DeleteDataModalComponent) modal: DeleteDataModalComponent;
  @ViewChild(PaginationComponent) pagination: PaginationComponent;

  deleteThreadStrategyHandler: DeleteThreadStrategyHandler;

  forumList;
  constructor(
    private threadService: ThreadService,
    private forumService: ForumService,
    private apiService: ApiService,
    private userSessionService: UserSessionService
  ) { }

  ngOnInit() {
    this.forumList = FORUM_DETAIL_LIST;
    this.deleteThreadStrategyHandler = new DeleteThreadStrategyHandler();
  }

  userIsAdmin(): boolean {
    return this.userSessionService.isAdmin;
  }

  unlockThread(entry: ThreadListEntryView) {
    this.threadService.patchUnlockThread(entry.id, !entry.unlocked).subscribe(
      (response) => entry.unlocked = !entry.unlocked,
      e => console.error(e)
    );
  }

  archiveThread(thread: ThreadListEntryView) {
    const lastThread = this.view.threads.length === 1;
    const lastPage = this.view.paging.maxPageCount === 1;
    this.threadService.patchArchiveThread(thread.id).subscribe(
      (response) => {
        this.handleDeleteResponse(lastThread, lastPage, thread.id);
        this.archiveThreadEvent.emit();
      },
      e => console.error(e)
    );
  }

  openModal(thread: ThreadListEntryView) {
    this.modal.showErrorMessage({ id: thread.id, name: thread.title });
  }

  deleteThread(threadId) {
    const hardDelete = this.userIsAdmin() ? true : false;
    const lastThread = this.view.threads.length === 1;
    const onlyOnePage = this.view.paging.totalRecordCount <= ItemsPerPage;
    this.threadService.delete(threadId, hardDelete).subscribe(
      response => {
        const threadIndex = this.view.threads.map(t => t.id).indexOf(threadId);
        if (hardDelete) {
          this.handleDeleteResponse(lastThread, onlyOnePage, threadId);
        } else {
          this.view.threads[threadIndex].isDeleted = true;
        }
        this.modal.hide();
      },
      e => console.error(e)
    );
  }

  onPagingByNumberEvent($event: PagingParameter) {
    this.forumService.getForumThreads(this.forumId, 'ACTIVE', $event).subscribe(
      response => {
        this.view = response;
        this.pagination.handlePagingByNumberResponse($event.pageNumber);
      },
      e => console.error(e)
    );
  }

  handlePagingResponseEvents($event: ThreadListView) {
    this.view = $event;
  }

  hasNoThreads() {
    return !this.view.threads.length;
  }

  private handleDeleteResponse(wasLastThreadInPage: boolean, onlyOnePage: boolean, threadId: number) {
    const strategy = this.deleteThreadStrategyHandler.getRightStrategy(wasLastThreadInPage, onlyOnePage);

    switch (strategy.constructor) {
      case LoadPreviousPage:
        this.pagination.pagingPrev(true);
        break;
      case SpliceThreadsArray:
        this.view.threads = (strategy as SpliceThreadsArray)
          .handleResponse(this.view.threads, threadId);
        --this.view.paging.totalRecordCount;
        break;
      case QueryThreads:
        (strategy as QueryThreads)
          .handleResponse(this.pagination.currentPage, this.forumId, 'ACTIVE', this.forumService)
          .subscribe(response => {
            this.view = response;
            this.pagination.updatePaging(response.paging);
          });
        break;
      default:
        break;
    }
  }

}
