export interface ForumDetails {
    forumName: string;
    topic: string;
    description: string;
    creator: string;
    lastActivity: string;
    status: string;
}

export const FORUM_DETAIL_LIST: ForumDetails[] = [
    // tslint:disable-next-line:max-line-length
    { forumName: 'NTAG Dingens Forum', topic: 'Coolere Farben', description: 'Die Farben unserer Firma sind ein bisschen langweilig. Das sollten wir ändern.', creator: 'Susi Sorglos', lastActivity: '15.12.2017', status: 'In Prüfung' },
    // tslint:disable-next-line:max-line-length
    { forumName: 'Projekt xY Forum', topic: 'Längere Mittagspause', description: 'In einer halben Stunde kann ich meinen Teller nicht leeren. Deshalb muss ich immer schlingen , um mein Mittag auf zu essen. Ich bekomme davon aber Bauchschmerzen. Soweit ich weiß, geht es dem Herren Kunz ähnlich.', creator: 'Sebastian Metermann', lastActivity: '02.01.2018', status: 'Wird nicht umgesetzt' },
    // tslint:disable-next-line:max-line-length
    { forumName: 'NTAG rulez', topic: 'Effizientere Tools', description: 'ich habe ein paar Tools, die unsere Arbeit effizienter machen können. Diese würde ich gerne in einem Workshop vorstellen.', creator: 'Hinz Kunz', lastActivity: '03.01.2018', status: 'Wird umgesetzt' },
    // tslint:disable-next-line:max-line-length
    { forumName: 'Einfach so', topic: 'Neue Arbeitskleidung', description: 'ich brauche ein neues T-Shirt. Meines ist schon sehr blass.', creator: 'Susi Sorglos', lastActivity: '15.12.2017', status: 'In Prüfung' },
    // tslint:disable-next-line:max-line-length
    { forumName: 'Weil ich es kann', topic: 'Kostenloser Kaffee', description: 'Ich verlange kostenlosen Kaffee, da ich stetig 10 Stunden hier in der Firma arbeite.', creator: 'Hinz Kunz', lastActivity: '02.01.2018', status: 'Wird umgesetzt' },
    // tslint:disable-next-line:max-line-length
    { forumName: 'Morgen ist auch noch ein Tag', topic: 'Mehr Zeit für Weiterbildung', description: 'Ich habe viele interessante Workshops entdeckt, die mich in meiner Arbeit weiter bringen können. Allerdings habe ich nie Zeit dafür, da ich jeden Tag hier arbeiten muss.', creator: 'Susi Sorglos', lastActivity: '03.01.2018', status: 'Wird nicht umgesetzt.' }
];
