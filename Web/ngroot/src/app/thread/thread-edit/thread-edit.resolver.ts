import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ThreadService } from '../thread.service';

@Injectable()
export class ThreadEditResolver implements Resolve<any> {
    constructor(
        private threadService: ThreadService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.threadService.get(route.parent.params['threadId']);
    }
}
