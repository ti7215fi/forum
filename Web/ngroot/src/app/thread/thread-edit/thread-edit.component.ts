import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ThreadService } from '../thread.service';
import { QuillEditorComponent } from 'ngx-quill';
import { QUILL_EDITOR_MODULES } from '../../app-common/quill-editor-options';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-thread-edit',
  templateUrl: './thread-edit.component.html',
  styleUrls: ['./thread-edit.component.css']
})
export class ThreadEditComponent implements OnInit {

  @ViewChild(QuillEditorComponent) editor: QuillEditorComponent;
  editorModules = QUILL_EDITOR_MODULES;

  threadId: number;
  view: ThreadEditResponse;
  request: ThreadEditRequest;
  editorOptions: any;
  showErrorMessage: boolean;
  showSuccessMessage: boolean;

  // Validation
  rForm: FormGroup;
  title: FormControl;
  editDescription: FormControl;
  titleAlert = 'Dies ist ein Pflichfeld!';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private threadService: ThreadService
  ) { }

  ngOnInit() {
    this.initData();
    this.createFormControls();
    this.createForm();
  }

  update() {
    this.request.descriptionPlain = this.editor.editorElem.textContent;
    this.threadService.update(this.threadId, this.request).subscribe(
      promise => {
        this.showErrorMessage = false;
        this.showSuccessMessage = true;
      },
      e => {
        this.showSuccessMessage = false;
        this.showErrorMessage = true;
        console.error(e);
      }
    );
  }

  private initData() {
    this.view = this.route.snapshot.data['thread'];
    this.request = {
      descriptionHtml: this.view.description,
      descriptionPlain: '',
      title: this.view.title,
      unlock: this.view.unlock
    };
    this.route.parent.params.subscribe(params => {
      this.threadId = params['threadId'];
    });
    this.showErrorMessage = false;
    this.showSuccessMessage = false;
  }

  private createFormControls() {
    this.title = new FormControl('', Validators.required);
    this.editDescription = new FormControl('', Validators.required);
  }

  private createForm() {
    this.rForm = new FormGroup({
      title: this.title,
      editDescription: this.editDescription
    });
  }

}
