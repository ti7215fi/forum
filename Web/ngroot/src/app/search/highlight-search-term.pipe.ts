import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'highlightSearchTerm'
})
export class HighlightSearchTermPipe implements PipeTransform {

  transform(text: string, searchTerm: string): any {
    const re = new RegExp(searchTerm, 'gi');
    return text.replace(re, '<mark>' + searchTerm + '</mark>');
  }

}
