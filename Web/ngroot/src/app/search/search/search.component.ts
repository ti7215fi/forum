import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Output() searchEvent = new EventEmitter<string>();

  searchTerm: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  search() {
    if (!this.router.url.match('/search-result')) {
      this.router.navigate(['/search-result', this.searchTerm]);
    } else {
      this.searchEvent.emit(this.searchTerm);
    }
  }

}
