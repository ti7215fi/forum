declare interface SearchResult {
    id: number;
    forumId: number;
    heading: string;
    creator: string;
    description: string;
    forumName: string;
}

declare interface ThreadSearchResult extends SearchResult {

}

declare interface PostSearchResult extends SearchResult {
    threadId: number;
}

declare interface SearchResults {
    searchTerm: string;
    count: number;
    threads: ThreadSearchResult[];
    posts: PostSearchResult[];
}