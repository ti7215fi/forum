import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { AppRoutingModule } from '../app-routing/app-routing.module';
import { SearchService } from './search.service';
import { SearchResultResolver } from './search-result/search-result.resolver';
import { FormsModule } from '@angular/forms';
import { HighlightSearchTermPipe } from './highlight-search-term.pipe';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule
  ],
  exports: [
    SearchComponent,
    SearchResultComponent
  ],
  declarations: [SearchComponent, SearchResultComponent, HighlightSearchTermPipe],
  providers: [SearchService, SearchResultResolver]
})
export class SearchModule { }
