import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router/src/interfaces';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SearchService } from '../search.service';

@Injectable()
export class SearchResultResolver implements Resolve<any> {
    constructor(
        private searchService: SearchService
    ) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.searchService.search(route.params['searchTerm']);
    }
}
