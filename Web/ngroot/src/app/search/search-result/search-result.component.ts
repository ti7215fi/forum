import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {

  searchResults: SearchResults;
  filterByPosts: boolean;
  filterByThreads: boolean;

  constructor(
    private searchService: SearchService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.searchResults = this.route.snapshot.data['searchResults'];
    this.filterByPosts = true;
    this.filterByThreads = true;
  }

  updateResults($event: string) {
    this.searchService.search($event).subscribe(response => {
      this.searchResults = response;
    }, e => console.error(e));
  }

}
