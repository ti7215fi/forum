import { Injectable } from '@angular/core';
import { ApiService } from '../app-common/api.service';

@Injectable()
export class SearchService {

  private readonly API_ROOT_URL: string = 'search';

  constructor(
    private apiService: ApiService
  ) { }

  search(searchTerm: string) {
    return this.apiService.authGet(this.API_ROOT_URL + '?searchTerm=' +  searchTerm);
  }

}
